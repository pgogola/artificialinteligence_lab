from math import sqrt, pow
import numpy as np
import random


class Node():
    def __init__(self, id, x, y):
        self.id = id
        self.x = x
        self.y = y
        self.items = []


class Item():
    def __init__(self, id, prof, weight, node_num):
        self.id = id
        self.prof = prof
        self.weight = weight
        self.node_num = node_num


class Individual():
    def __init__(self, route, fitness=0):
        self.route = route
        self.fitness = fitness


def knp_sort(items):
    items.sort(key=lambda it: it.prof/it.weight, reverse=True)
    return items


def knp_select(nodes, route, max_capacity):
    selected_items = []
    current_load = 0
    for r in route:
        for i in nodes[r-1].items:
            if current_load + i.weight <= max_capacity:
                current_load += i.weight
                selected_items.append(i)
                break
    return selected_items


def g_function(selected_items):
    return np.sum(i.prof for i in selected_items)


def f_function(selected_items, route, distances, v_max, v_min, max_capacity):
    v_diff = v_max-v_min
    current_weight = 0
    current_speed = v_max
    route_points_size = len(route)
    items_size = len(selected_items)
    r_idx = 0
    i_idx = 0
    f_result = 0

    def calc_speed(w_c):
        return v_max - w_c*v_diff/max_capacity

    while r_idx < route_points_size - 1:
        if i_idx < items_size and selected_items[i_idx].node_num == route[r_idx]:
            current_weight += selected_items[i_idx].weight
            current_speed = calc_speed(current_weight)
            i_idx += 1
        f_result += distances[route[r_idx]-1][route[r_idx+1]-1]/current_speed
        r_idx += 1

    if i_idx < items_size and selected_items[i_idx].node_num == route[r_idx]:
        current_weight += selected_items[i_idx].weight
        current_speed = calc_speed(current_weight)

    f_result += distances[route[r_idx]-1][route[0]-1]/current_speed
    return f_result


def G_function(selected_items, route, distances, v_max, v_min, max_capacity):
    return g_function(selected_items) - f_function(selected_items, route, distances, v_max, v_min, max_capacity)


def mutation(individual, mutation_rate):
    for i in range(len(individual)):
        if(random.random() <= mutation_rate):
            other_index_swap = random.randint(0, len(individual)-1)
            individual[i], individual[other_index_swap] = individual[other_index_swap], individual[i]
    return individual


def PMX(individual1, individual2):
    cross_idx_1 = random.randint(0, len(individual1))
    cross_idx_2 = random.randint(0, len(individual1))
    idx_start = min(cross_idx_1, cross_idx_2)
    idx_stop = max(cross_idx_1, cross_idx_2)
    ind1_part = individual1[idx_start:idx_stop]
    ind2_part = individual2[idx_start:idx_stop]

    def create_child(indiv1, indiv2, indiv_part1, indiv_part2, start_idx, stop_idx):
        child = indiv2[:start_idx]+indiv_part1+indiv2[stop_idx:]
        i = 0
        diff = list(set(indiv_part2)-set(indiv_part1))
        for v in diff:
            v_p1 = indiv_part1[indiv_part2.index(v)]
            while v_p1 in indiv_part2:
                v_p1 = indiv_part1[indiv_part2.index(v_p1)]
            child[indiv2.index(v_p1)] = v
        return child

    return (create_child(individual1, individual2, ind1_part, ind2_part, idx_start, idx_stop),
            create_child(individual2, individual1, ind2_part, ind1_part, idx_start, idx_stop))


def calculate_distance(node1, node2):
    return sqrt(pow(node1.x - node2.x, 2) + pow(node1.y - node2.y, 2))


def getDistancesArray(nodes):
    size = len(nodes)
    distances = np.empty([size, size])
    for i in nodes:
        for j in nodes:
            distances[i.id-1][j.id-1] = calculate_distance(i, j)
    return distances


def selection_tournament(population, tournament_size):
    result_population = []
    for i in range(len(population)):
        tourn = random.sample(population, k=tournament_size)
        result_population.append(max(tourn, key=lambda x: x.fitness))
    return result_population


def selection_roulette(population):
    min_fit_value = abs(min(population, key=lambda x: x.fitness).fitness)+1
    sum_fitness = 0
    roulette = {}
    result_population = []
    for p in population:
        p.fitness += min_fit_value
        sum_fitness += (p.fitness + min_fit_value)
        roulette[sum_fitness] = p
    for i in range(len(population)):
        rand = random.uniform(0, sum_fitness)
        for key, individual in roulette.items():
            if key > rand:
                result_population.append(individual)
                break
    return result_population


def selection_roulette_exp(population):
    min_fit_value = abs(min(population, key=lambda x: x.fitness).fitness)+1
    max_fit_value = max(
        population, key=lambda x: x.fitness).fitness+min_fit_value
    sum_fitness = np.sum([p.fitness+min_fit_value for p in population])
    roulette = {}
    result_population = []
    sum_result = 0
    for p in population:
        sum_result += (p.fitness + min_fit_value)*100/sum_fitness
        roulette[sum_result] = p
    for i in range(len(population)):
        rand = random.uniform(0, sum_result)
        for key, individual in roulette.items():
            if key > rand:
                result_population.append(individual)
                break
    return result_population


def check_and_fix(route):
    return route


class GeneticComputation():
    def __init__(self, input_data, population_size, mutation_ratio,
                 crossover_ratio, tournament_size, generations, logger=None):
        self.__population = []
        self.__population_size = population_size
        self.__v_max = input_data['max_speed']
        self.__v_min = input_data['min_speed']
        self.__knapsack_capacity = input_data['knapsack_capacity']
        self.__nodes = input_data['nodes']
        self.__items = input_data['items']
        self.__distances = getDistancesArray(self.__nodes)
        self.__previous_results = {}
        self.__mutation_ratio = mutation_ratio
        self.__crossover_ratio = crossover_ratio
        self.__tournament_size = tournament_size
        self.__generations = generations
        self.__logger = logger

        self.__the_best_fitness = float('-Inf')
        self.__the_best_indiv = None

        for n in self.__nodes:
            knp_sort(n.items)

        self.g_calc = 0

    def init_random_individual(self):
        return random.sample(range(1, len(self.__nodes)+1), len(self.__nodes))

    def evaluate(self):
        for i in self.__population:
            if str(i.route) in self.__previous_results:
                i.fitness = self.__previous_results[str(i.route)]
            else:
                i.fitness = G_function(knp_select(self.__nodes, i.route, self.__knapsack_capacity), i.route,
                                       self.__distances, self.__v_max, self.__v_min, self.__knapsack_capacity)
                self.__previous_results[str(i.route)] = i.fitness
                self.g_calc += 1
            if i.fitness > self.__the_best_fitness:
                self.__the_best_fitness = i.fitness
                self.__the_best_indiv = np.copy(i.route)

    def crossover_population(self):
        new_population = []
        for i in range(self.__population_size):
            p1, p2 = random.sample(self.__population, k=2)
            ch1, ch2 = PMX(p1.route, p2.route)
            new_population.append(Individual(ch1))
            # new_population.append(Individual(ch2))
        return new_population

    def mutate_population(self):
        new_population = []
        for i in self.__population:
            new_population.append(Individual(
                mutation(i.route, self.__mutation_ratio)))
        return new_population

    def check_and_fix_population(self):
        for i in self.__population:
            i.route = check_and_fix(i.route)

    def run_alg(self):
        self.__population = [
            Individual(self.init_random_individual()) for i in range(self.__population_size)]
        self.evaluate()
        for e in range(self.__generations):
            # self.__population = selection_tournament(
            #     self.__population, self.__tournament_size)
            self.__population = selection_roulette_exp(self.__population)
            self.__population = self.crossover_population()
            self.__population = self.mutate_population()
            self.evaluate()
            if not self.__logger == None:
                max_f = max(self.__population, key=lambda x: x.fitness).fitness
                min_f = min(self.__population, key=lambda x: x.fitness).fitness
                avg_f = np.sum(
                    [p.fitness for p in self.__population])/len(self.__population)
                self.__logger.log(f'{e}, {max_f}, {min_f}, {avg_f}\n')
