import loader as lr
import computation as cp
import logger as lg
import os


class Param():
    def __init__(self, population_size, mutation_ratio, crossover_ratio, tournament_size, generation_size):
        self.population_size = population_size
        self.mutation_ratio = mutation_ratio
        self.crossover_ratio = crossover_ratio
        self.tournament_size = tournament_size
        self.generation_size = generation_size


if __name__ == '__main__':

    input_file_names = ["hard_0", "hard_1", "hard_2", "hard_3", "hard_4"]
    population_sizes = [100, 500, 1000]
    mutation_ratios = [0.01, 0.10, 0.50]
    crossover_ratios = [0.1, 0.4, 0.8]
    tournament_sizes = [5, 25, 50]
    generations_size = [100, 500, 1000]

    params = [Param(population_size=100, mutation_ratio=0.01,
                    crossover_ratio=0.1, tournament_size=5, generation_size=100),
              Param(population_size=500, mutation_ratio=0.01,
                    crossover_ratio=0.1, tournament_size=5, generation_size=100),
              Param(population_size=1000, mutation_ratio=0.01,
                    crossover_ratio=0.1, tournament_size=5, generation_size=100), ]

    params = [Param(population_size=250, mutation_ratio=0.01,
                    crossover_ratio=0.7, tournament_size=5, generation_size=100),
              Param(population_size=250, mutation_ratio=0.1,
                    crossover_ratio=0.7, tournament_size=5, generation_size=100),
              Param(population_size=250, mutation_ratio=0.25,
                    crossover_ratio=0.7, tournament_size=5, generation_size=100),

              Param(population_size=250, mutation_ratio=0.01,
                    crossover_ratio=0.25, tournament_size=5, generation_size=100),
              Param(population_size=250, mutation_ratio=0.01,
                    crossover_ratio=0.5, tournament_size=5, generation_size=100),
              Param(population_size=250, mutation_ratio=0.01,
                    crossover_ratio=0.7, tournament_size=5, generation_size=100),

              Param(population_size=250, mutation_ratio=0.01,
                    crossover_ratio=0.7, tournament_size=5, utation_ratio=0.01,
                    crossover_ratio=0.7, tournament_size=5, generation_size=500),
              Param(population_size=250, mutation_ratio=0.01,
                    crossover_ratio=0.7, tournament_size=5, generation_size=1000),

              Param(population_size=250, mutation_ratio=0.01,
                    crossover_ratio=0.7, tournament_size=5, generation_size=100),
              Param(population_size=250, mutation_ratio=0.01,
                    crossover_ratio=0.7, tournament_size=25, generation_size=100),
              Param(population_size=250, mutation_ratio=0.01,
                    crossover_ratio=0.7, tournament_size=50, generation_size=100), ]

    for ifn in input_file_names:
        for p in params:
            filename = "../input_data/"+ifn+".ttp"
            population_size = p.population_size
            mutation_ratio = p.mutation_ratio
            crossover_ratio = p.crossover_ratio
            tournament_size = p.tournament_size
            generations = p.generation_size
            input_data = lr.loader(filename)
            for it in range(5):
                exists = os.path.isfile(
                    './results_turniej/' + f'ifn{ifn}ps{population_size}mr{mutation_ratio}cr{crossover_ratio}ts{tournament_size}g{generations}_{it}.csv')
                if exists:
                    continue
                log = lg.Logger(
                    output_file='./results_turniej/' + f'ifn{ifn}ps{population_size}mr{mutation_ratio}cr{crossover_ratio}ts{tournament_size}g{generations}_{it}.csv')
                log.open_file()
                genetic = cp.GeneticComputation(input_data, population_size, mutation_ratio,
                                                crossover_ratio, tournament_size, generations,
                                                log)
                genetic.run_alg()
                print(genetic.g_calc)
                log.close_file()
