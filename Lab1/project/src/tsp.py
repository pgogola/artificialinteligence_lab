import loader as lr
import computation as cp
import logger as lg
import random


def tsp_nearest_neighbour_distances(distances, start_node, nodes):
    result = [start_node]
    visited = {start_node}
    to_visit = set(nodes)
    to_visit.remove(start_node)
    current_node = start_node
    while not len(to_visit) == 0:
        distance = float('inf')
        next_node = None
        for tv in to_visit:
            if distances[current_node-1][tv-1] < distance:
                distance = distances[current_node-1][tv-1]
                next_node = tv
        result.append(next_node)
        to_visit.remove(next_node)
        visited.add(next_node)
        current_node = next_node

    return result


def tsp_nearest_neighbour(distances, start_node, nodes_num, nodes, max_capacity, v_max, v_min):
    v_diff = v_max-v_min

    def calc_speed(w_c):
        return v_max - w_c*v_diff/max_capacity

    result = [start_node]
    visited = {start_node}
    to_visit = set(nodes_num)
    to_visit.remove(start_node)
    current_node = start_node
    current_load = 0
    for i in nodes[start_node-1].items:
        if current_load + i.weight <= max_capacity:
            current_load += i.weight
            break
    while not len(to_visit) == 0:
        time = float('inf')
        next_node = None
        for tv in to_visit:
            curr_time = distances[current_node -
                                  1][tv-1] / calc_speed(current_load)
            if curr_time < time:
                time = curr_time
                next_node = tv
        for i in nodes[next_node-1].items:
            if current_load + i.weight <= max_capacity:
                current_load += i.weight
                break
        result.append(next_node)
        to_visit.remove(next_node)
        visited.add(next_node)
        current_node = next_node

    return result


files = ["hard_0", "hard_1", "hard_2", "hard_3", "hard_4"]

for f in files:
    filename = "../input_data/"+f+".ttp"
    input_data = lr.loader(filename)

    v_max = input_data['max_speed']
    v_min = input_data['min_speed']
    knapsack_capacity = input_data['knapsack_capacity']
    nodes = input_data['nodes']
    items = input_data['items']
    distances = cp.getDistancesArray(nodes)
    for n in nodes:
        cp.knp_sort(n.items)
    nodes_num = [n.id for n in nodes]
    the_best_result = float('-inf')
    route = None
    the_best_route = None
    for nn in range(1, 6):
        # route = tsp_nearest_neighbour(distances=distances, start_node=nn, nodes_num=nodes_num,
        #                               nodes=nodes, max_capacity=knapsack_capacity, v_max=v_max, v_min=v_min)
        route = tsp_nearest_neighbour_distances(
            distances=distances, start_node=nn, nodes=nodes_num)

        r = cp.G_function(cp.knp_select(nodes, route, knapsack_capacity), route,
                          distances, v_max, v_min, knapsack_capacity)
        print(cp.knp_select(nodes, route, knapsack_capacity))
        if r > the_best_result:
            the_best_route = route
            the_best_result = r
        with open('tsp_res/'+f+".txt", 'a+') as res:
            res.write(str(the_best_route) + '\n')
            res.write(str(the_best_result) + '\n')
            print(the_best_route)
