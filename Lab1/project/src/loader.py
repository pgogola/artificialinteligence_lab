from computation import Item, Node


def loader(file_name):
    PROBLEM_NAME = "PROBLEM NAME:"
    KNAPSACK_DATA_TYPE = "KNAPSACK DATA TYPE:"
    DIMENSION = "DIMENSION:"
    NUMBER_OF_ITEMS = "NUMBER OF ITEMS:"
    CAPACITY_OF_KNAPSACK = "CAPACITY OF KNAPSACK:"
    MIN_SPEED = "MIN SPEED:"
    MAX_SPEED = "MAX SPEED:"
    RENTING_RATIO = "RENTING RATIO:"
    EDGE_WEIGHT_TYPE = "EDGE_WEIGHT_TYPE:"
    NODE_COORD_SECTION = "NODE_COORD_SECTION"
    ITEMS_SECTION = "ITEMS SECTION"

    def remove(old, str): return " ".join(str.replace(old, '').split())
    result_data = {}

    nodes_flag = False
    items_flag = False

    nodes = []
    items = []
    nodes_d = {}

    with open(file_name) as input_data:
        for line in input_data:
            if line.startswith(PROBLEM_NAME):
                result_data['problem_name'] = remove(PROBLEM_NAME, line)
            elif line.startswith(KNAPSACK_DATA_TYPE):
                result_data['knapsact_type'] = remove(KNAPSACK_DATA_TYPE, line)
            elif line.startswith(DIMENSION):
                result_data['dimension'] = int(remove(DIMENSION, line))
            elif line.startswith(NUMBER_OF_ITEMS):
                result_data['items_num'] = int(remove(NUMBER_OF_ITEMS, line))
            elif line.startswith(CAPACITY_OF_KNAPSACK):
                result_data['knapsack_capacity'] = int(
                    remove(CAPACITY_OF_KNAPSACK, line))
            elif line.startswith(MIN_SPEED):
                result_data['min_speed'] = float(remove(MIN_SPEED, line))
            elif line.startswith(MAX_SPEED):
                result_data['max_speed'] = float(remove(MAX_SPEED, line))
            elif line.startswith(RENTING_RATIO):
                result_data['renting_ratio'] = float(
                    remove(RENTING_RATIO, line))
            elif line.startswith(EDGE_WEIGHT_TYPE):
                result_data['edge_type'] = remove(EDGE_WEIGHT_TYPE, line)
            elif line.startswith(NODE_COORD_SECTION):
                nodes_flag = True
                items_flag = False
                continue
            elif line.startswith(ITEMS_SECTION):
                nodes_flag = False
                items_flag = True
                continue

            if nodes_flag:
                params = line.split()
                node = Node(int(params[0]), int(
                    float(params[1])), int(float(params[2])))
                nodes.append(node)
                nodes_d[node.id] = node

            if items_flag:
                params = line.split()
                item = Item(int(params[0]), int(
                    params[1]), int(params[2]), int(params[3]))
                items.append(item)
                nodes_d[item.node_num].items.append(item)

    result_data['nodes'] = nodes
    result_data['items'] = items

    return result_data
