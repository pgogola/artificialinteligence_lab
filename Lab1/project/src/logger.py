class Logger():
    def __init__(self, output_file):
        self.__output_file = output_file
        self.__file_handler = None

    def open_file(self):
        self.__file_handler = open(self.__output_file, 'w')

    def close_file(self):
        self.__file_handler.close()
        self.__file_handler = None

    def log(self, data):
        self.__file_handler.write(data)
