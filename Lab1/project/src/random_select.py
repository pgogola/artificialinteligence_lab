import loader as lr
import computation as cp
import logger as lg
import random
from itertools import permutations


def tsp_random(distances, nodes_num, nodes, generations, v_max, v_min, knapsack_capacity):
    the_best_solution = None
    the_best_fitness = float("-inf")
    length = len(nodes_num)

    for i in range(generations):
        rand_ind = random.sample(nodes_num, length)
        G_value = cp.G_function(cp.knp_select(nodes, rand_ind, knapsack_capacity), rand_ind,
                                distances, v_max, v_min, knapsack_capacity)
        if G_value > the_best_fitness:
            the_best_fitness = G_value
            the_best_solution = rand_ind
    return the_best_solution, the_best_fitness


files = ["hard_0", "hard_1", "hard_2", "hard_3", "hard_4"]

for f in files:
    filename = "../input_data/"+f+".ttp"
    input_data = lr.loader(filename)

    v_max = input_data['max_speed']
    v_min = input_data['min_speed']
    knapsack_capacity = input_data['knapsack_capacity']
    nodes = input_data['nodes']
    items = input_data['items']
    distances = cp.getDistancesArray(nodes)
    for n in nodes:
        cp.knp_sort(n.items)
    nodes_num = [n.id for n in nodes]

    route, fitness = tsp_random(
        distances, nodes_num, nodes, 25000, v_max, v_min, knapsack_capacity)
    with open('random/'+f+".txt", 'a+') as res:
        res.write(str(route) + '\n')
        res.write(str(fitness))
