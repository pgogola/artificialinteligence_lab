import computation as cp


ind1 = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
ind2 = [8, 4, 7, 3, 6, 2, 5, 1, 9, 0]


print(cp.PMX(ind1, ind2))

prof = 21
weight = 21

selected_items = [
    cp.Item(1, 10, 10, 1),
    cp.Item(4, 5, 5, 2),
    cp.Item(3, 1, 1, 3),
    cp.Item(2, 3, 3, 4),
    cp.Item(5, 2, 2, 5)
]

print(prof == cp.g_function(selected_items))

distances = [[0, 1, 1, 1, 1],
             [1, 0, 1, 1, 1],
             [1, 1, 0, 1, 1],
             [1, 1, 1, 0, 1],
             [1, 1, 1, 1, 0]]

f_val = 5
print(f_val == cp.f_function(selected_items,
                             [1, 2, 3, 4, 5], distances, 1, 0.5, 100))
print(cp.f_function(selected_items,
                    [1, 2, 3, 4, 5], distances, 1, 0.5, 100))
