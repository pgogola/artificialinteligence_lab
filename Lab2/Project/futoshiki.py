import numpy as np
from itertools import permutations
import time


class Futoshiki():
    def __init__(self, size):
        self.size = size
        self.board = np.empty([size, size], dtype=int)
        self.relations_pairs = []
        self.results = []
        self.permutations = None
        self.argindic = []

    def check_row_unique(self, board, row_number):
        urow = set(board[row_number])
        urow.discard(0)
        return len(urow) == np.count_nonzero(board[row_number])

    def check_col_unique(self, board, col_number, row):
        x = board[:, col_number]
        ucol = set(x)
        ucol.discard(0)
        return len(ucol) == np.count_nonzero(x != 0)

    def check_pair(self, board, row1, col1, row2, col2):
        v1 = board[row1, col1]
        v2 = board[row2, col2]
        relation = self.get_constraint(row1, col1, row2, col2)
        return 0 == v1 or 0 == v2 or 0 == relation or (v1-v2 < 0 if relation == -1 else v1-v2 > 0)

    def backtracking(self, heurestic="biggest_fill"):
        found_solutions = 0
        recurrences = 0
        nodes = 0

        def biggest_filled_up_cells_heurestic():
            number_of_contraints_in_row = []
            for r in self.board:
                number_of_contraints_in_row.append(-np.count_nonzero(r))
            return np.argsort(number_of_contraints_in_row)

        def lowest_filled_up_cells_heurestic():
            number_of_contraints_in_row = []
            for r in self.board:
                number_of_contraints_in_row.append(np.count_nonzero(r))
            return np.argsort(number_of_contraints_in_row)

        def biggest_gl_constraint_heurestic():
            constraints = np.zeros(self.size)

            for p in self.relations_pairs:
                row1 = p[0]
                row2 = p[2]
                constraints[row1] -= 1
                constraints[row2] -= 1

            return np.argsort(constraints)

        def lowest_gl_constraint_heurestic():
            constraints = np.zeros(self.size)

            for p in self.relations_pairs:
                row1 = p[0]
                row2 = p[1]
                constraints[row1] += 1
                constraints[row2] += 1

            return np.argsort(constraints)

        def check_constraints(board, row, size, row_i):
            indx = []
            for i in range(len(self.relations_pairs)):
                if self.relations_pairs[i][0] == row or self.relations_pairs[i][2] == row:
                    indx.append(i)
            if len(indx) != 0:
                for p in [self.relations_pairs[i] for i in indx]:
                    v1 = board[p[0], p[1]]
                    v2 = board[p[2], p[3]]
                     if 0 != v1 and 0 != v2 and not v1-v2 < 0:
                        return False
            for i in range(size):
                if not self.check_col_unique(board, i, row_i):
                    return False

            return True

        def backtracking_recursive(b, row, order):
            nonlocal recurrences
            nonlocal found_solutions
            nonlocal nodes
            if row >= self.size:
                found_solutions += 1
                return
           current = np.copy(b)
            real_row = order[row]
            for p in self.permutations:
                indeces = self.argindic[real_row]

                if sum(abs(self.board[real_row, indeces] - p[indeces])) == 0:
                    current[real_row] = p
                    nodes += 1
                    if check_constraints(current, real_row, self.size, row):
                        backtracking_recursive(current, row+1, order)
                    else:
                        recurrences += 1
                else:
                    recurrences += 1

        self.permutations = np.array(
            [p for p in permutations(range(1, self.size+1))])

        def funH(): return range(self.size)

        if "biggest_fill" == heurestic:
            funH = biggest_filled_up_cells_heurestic
        elif "lowest_fill" == heurestic:
            funH = lowest_filled_up_cells_heurestic
        elif "biggest_con" == heurestic:
            funH = biggest_gl_constraint_heurestic
        elif "lowest_con" == heurestic:
            funH = lowest_gl_constraint_heurestic

        order_of_search = funH()
        start = time.time()
        backtracking_recursive(self.board, 0, order_of_search)
        stop = time.time()
        return (found_solutions, nodes, recurrences, stop-start)

    def forwardtracking(self, heurestic="biggest_fill"):
        found_solutions = 0
        recurrences = 0
        nodes = 0

        def biggest_filled_up_cells_heurestic():
            number_of_contraints_in_row = []
            for r in self.board:
                number_of_contraints_in_row.append(-np.count_nonzero(r))
            return np.argsort(number_of_contraints_in_row)

        def lowest_filled_up_cells_heurestic():
            number_of_contraints_in_row = []
            for r in self.board:
                number_of_contraints_in_row.append(np.count_nonzero(r))
            return np.argsort(number_of_contraints_in_row)

        def biggest_gl_constraint_heurestic():
            constraints = np.zeros(self.size)

            for p in self.relations_pairs:
                row1 = p[0]
                row2 = p[2]
                constraints[row1] -= 1
                constraints[row2] -= 1

            return np.argsort(constraints)

        def lowest_gl_constraint_heurestic():
            constraints = np.zeros(self.size, dtype=int)

            for p in self.relations_pairs:
                row1 = p[0]
                row2 = p[2]
                constraints[row1] += 1
                constraints[row2] += 1

            return np.argsort(constraints)

        def check_constraints(board, row, size, row_i):
            indx = []
            for i in range(len(self.relations_pairs)):
                if self.relations_pairs[i][0] == row or self.relations_pairs[i][2] == row:
                    indx.append(i)
            if len(indx) != 0:
                for p in [self.relations_pairs[i] for i in indx]:
                    v1 = board[p[0], p[1]]
                    v2 = board[p[2], p[3]]
                     if 0 != v1 and 0 != v2 and not v1-v2 < 0:
                        return False
            for i in range(size):
                if not self.check_col_unique(board, i, row_i):
                    return False

            return True

        def forwardtracking_recursive(b, row, order, ap):
            nonlocal recurrences
            nonlocal found_solutions
            nonlocal nodes
            if row >= self.size:
                found_solutions = found_solutions + 1
                return
            current = np.copy(b)
            real_row = order[row]
            for p in ap:
                indeces = self.argindic[real_row]
                if sum(abs(self.board[real_row, indeces] - self.permutations[p][indeces])) == 0:
                    current[real_row] = self.permutations[p]
                    nodes += 1
                    if check_constraints(current, real_row, self.size, row):
                        res = ap - \
                            set(np.argwhere(np.any(self.permutations -
                                                   self.permutations[p] == 0, axis=1)).flatten())
                        forwardtracking_recursive(
                            current, row+1, order, res)
                    else:
                        recurrences += 1
                else:
                    recurrences += 1

        self.permutations = np.array(
            [p for p in permutations(range(1, self.size+1))])
        array_of_permutations = set(
            [p for p in range(len(self.permutations))])

        def funH(): return range(self.size)

        if "biggest_fill" == heurestic:
            funH = biggest_filled_up_cells_heurestic
        elif "lowest_fill" == heurestic:
            funH = lowest_filled_up_cells_heurestic
        elif "biggest_con" == heurestic:
            funH = biggest_gl_constraint_heurestic
        elif "lowest_con" == heurestic:
            funH = lowest_gl_constraint_heurestic

        order_of_search = funH()

        start = time.time()
        forwardtracking_recursive(
            self.board, 0, order_of_search, array_of_permutations)
        stop = time.time()
        return (found_solutions, nodes, recurrences, stop-start)
