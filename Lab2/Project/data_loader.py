from futoshiki import Futoshiki
from skyscrapper import Skyscrapper
import numpy as np


def futoshiki_loader(file_name):
    futoshiki = None
    isStart = False
    isConstraint = False
    dataLine = 0
    def letter2num(x): return ord(x)-ord('A')
    with open(file_name, 'r') as f:
        for line in f:
            if None == futoshiki:
                futoshiki = Futoshiki(int(line))
            if "START:" in line:
                isStart = True
                isConstraint = False
                dataLine = 0
            elif "REL:" in line:
                isStart = False
                isConstraint = True
            elif isStart:
                splitted = line.split(";")
                assert len(splitted) == futoshiki.size
                li = []
                for i in range(futoshiki.size):
                    futoshiki.board[dataLine][i] = int(splitted[i])
                    if int(splitted[i]) > 0:
                        li.append(i)
                futoshiki.argindic.append(li)
                dataLine += 1
            elif isConstraint:
                splitted = line.split(";")
                row1 = letter2num(splitted[0][0])
                col1 = int(splitted[0][1])-1
                row2 = letter2num(splitted[1][0])
                col2 = int(splitted[1][1])-1
                futoshiki.relations_pairs.append((row1, col1, row2, col2))
    return futoshiki


def skyscrapper_loader(file_name):
    skyscrapper = None
    isStart = False
    isConstraint = False
    dataLine = 0
    def letter2num(x): return ord(x)-ord('A')
    with open(file_name, 'r') as f:
        for line in f:
            if None == skyscrapper:
                skyscrapper = Skyscrapper(int(line))
            else:
                splitted = line.split(";")
                row = None
                if splitted[0] == 'G':
                    row = skyscrapper.top
                elif splitted[0] == 'D':
                    row = skyscrapper.bottom
                elif splitted[0] == 'L':
                    row = skyscrapper.left
                elif splitted[0] == 'P':
                    row = skyscrapper.right
                for i in range(skyscrapper.size):
                    row[i] = int(splitted[i+1])

    return skyscrapper
