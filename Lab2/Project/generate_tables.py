import math
import numpy as np
import pickle


number = 3

b = None
with open("result_skyscrapper"+str(number)+".txt", "rb") as fp:   # Unpickling
    b = pickle.load(fp)


sky_files = [
    "test_sky_4_0.txt",
    "test_sky_4_1.txt",
    "test_sky_4_2.txt",
    "test_sky_4_3.txt",
    "test_sky_4_4.txt",
    "test_sky_5_0.txt",
    "test_sky_5_1.txt",
    "test_sky_5_2.txt",
    "test_sky_5_3.txt",
    "test_sky_5_4.txt",
    "test_sky_6_0.txt",
    "test_sky_6_1.txt",
    "test_sky_6_2.txt",
    "test_sky_6_3.txt",
    "test_sky_6_4.txt"
]

print("\\begin{table}[H]")
print("\\centering")
print("\\caption{Czas wykonania (sec)}")
print("\\begin{tabular}{l||c|c|c|c}")
print("Dane & Alg1 & Alg2 & Alg3 & Alg4 \\\\")
print("\\hline")
print("\\hline")
for file_name in sky_files:
    times = file_name.replace("_", "\_")
    for result in b:
        if result["filename"] == file_name:
            times += " & "
            times += "{:.4f}".format(np.mean(result["result"][3]))
    times += " \\\\\n\\hline"
    print(times)
print("\\end{tabular}")
print("\\end{table}\n\n\n")


print("\\begin{table}[H]")
print("\\centering")
print("\\caption{Liczba \\enquote{odwiedzonych} węzłów}")
print("\\begin{tabular}{l||c|c|c|c}")
print("Dane & Alg1 & Alg2 & Alg3 & Alg4 \\\\")
print("\\hline")
print("\\hline")
for file_name in sky_files:
    # print(file_name, end="")
    nodes = file_name.replace("_", "\_")
    for result in b:
        if result["filename"] == file_name:
            nodes += " & "
            nodes += str(np.max(result["result"][1]))
    nodes += " \\\\\n\\hline"
    print(nodes)
print("\\end{tabular}")
print("\\end{table}\n\n\n")

print("\\begin{table}[H]")
print("\\centering")
print("\\caption{Liczba \\enquote{nawrotów}}")
print("\\begin{tabular}{l||c|c|c|c}")
print("Dane & Alg1 & Alg2 & Alg3 & Alg4 \\\\")
print("\\hline")
print("\\hline")
for file_name in sky_files:
    # print(file_name, end="")
    recurences = file_name.replace("_", "\_")
    for result in b:
        if result["filename"] == file_name:
            recurences += " & "
            recurences += str(np.max(result["result"][2]))
    recurences += " \\\\\n\\hline"
    print(recurences)
print("\\end{tabular}")
print("\\end{table}\n\n\n")


b = None
with open("result_futoshiki"+str(number)+".txt", "rb") as fp:   # Unpickling
    b = pickle.load(fp)

futoshiki_files = [
    "test_futo_4_0.txt",
    "test_futo_4_1.txt",
    "test_futo_4_2.txt",
    "test_futo_5_0.txt",
    "test_futo_5_1.txt",
    "test_futo_5_2.txt",
    "test_futo_6_0.txt",
    "test_futo_6_1.txt",
    "test_futo_6_2.txt",
    "test_futo_7_0.txt",
    "test_futo_7_1.txt",
    "test_futo_7_2.txt",
    "test_futo_8_0.txt",
    "test_futo_8_1.txt",
    "test_futo_8_2.txt",
    "test_futo_9_0.txt",
    "test_futo_9_1.txt",
    "test_futo_9_2.txt"
]

print("\\begin{table}[H]")
print("\\centering")
print("\\caption{Czas wykonania (sec)}")
print("\\begin{tabular}{l||c|c|c|c}")
print("Dane & Alg1 & Alg2 & Alg3 & Alg4 \\\\")
print("\\hline")
print("\\hline")
for file_name in futoshiki_files:
    # print(file_name, end="")
    times = file_name.replace("_", "\_")
    for result in b:
        if result["filename"] == file_name:
            times += " & "
            times += "{:.4f}".format(np.mean(result["result"][3]))
    times += " \\\\\n\\hline"
    print(times)
print("\\end{tabular}")
print("\\end{table}\n\n\n")

print("\\begin{table}[H]")
print("\\centering")
print("\\caption{Liczba \\enquote{odwiedzonych} węzłów}")
print("\\begin{tabular}{l||c|c|c|c}")
print("Dane & Alg1 & Alg2 & Alg3 & Alg4 \\\\")
print("\\hline")
print("\\hline")
for file_name in futoshiki_files:
    # print(file_name, end="")
    nodes = file_name.replace("_", "\_")
    for result in b:
        if result["filename"] == file_name:
            nodes += " & "
            nodes += str(np.max(result["result"][1]))
    nodes += " \\\\\n\\hline"
    print(nodes)
print("\\end{tabular}")
print("\\end{table}\n\n\n")


print("\\begin{table}[H]")
print("\\centering")
print("\\caption{Liczba \\enquote{nawrotów}}")
print("\\begin{tabular}{l||c|c|c|c}")
print("Dane & Alg1 & Alg2 & Alg3 & Alg4 \\\\")
print("\\hline")
print("\\hline")
for file_name in futoshiki_files:
    # print(file_name, end="")
    recurences = file_name.replace("_", "\_")
    for result in b:
        if result["filename"] == file_name:
            recurences += " & "
            recurences += str(np.max(result["result"][2]))
    recurences += " \\\\\n\\hline"
    print(recurences)
print("\\end{tabular}")
print("\\end{table}\n\n\n")
