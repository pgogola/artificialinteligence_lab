import data_loader as dl
import futoshiki
import skyscrapper


def runTest(file_name, rounds, problem_type, used_heurestic, forward=True):
    times = []
    found_solutions = []
    recurrences = []
    nodes = []

    for i in range(rounds):
        problem = None
        if "futoshiki" == problem_type:
            problem = dl.futoshiki_loader(file_name)
        elif "skyscrapper" == problem_type:
            problem = dl.skyscrapper_loader(file_name)

        found_solutions_res = None
        nodes_res = None
        recurrences_res = None
        time_res = None
        if forward:
            found_solutions_res, nodes_res, recurrences_res, time_res = problem.forwardtracking(
                used_heurestic)
        else:
            found_solutions_res, nodes_res, recurrences_res, time_res = problem.backtracking(
                used_heurestic)

        found_solutions.append(found_solutions_res)
        nodes.append(nodes_res)
        recurrences.append(recurrences_res)
        times.append(time_res)
        print(file_name, end=" ")
        print(used_heurestic, end=" ")
        print(problem_type, end=" ")
        print(found_solutions_res, end=" ")
        print(nodes_res, end=" ")
        print(recurrences_res, end=" ")
        print(time_res, end=" ")
        print("\n")
    return found_solutions, nodes, recurrences, times
