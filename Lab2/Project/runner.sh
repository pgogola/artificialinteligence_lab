#!/bin/bash

for i in {1..6}
do
   echo "start $i"
   python3 main.py $i 1 &
   python3 main.py $i 2 &
   wait
   echo "stop $i"
done
