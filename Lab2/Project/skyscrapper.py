import numpy as np
from itertools import permutations
import time


class Skyscrapper():
    def __init__(self, size):
        self.size = size
        self.top = np.empty(size, dtype=int)
        self.bottom = np.empty(size, dtype=int)
        self.left = np.empty(size, dtype=int)
        self.right = np.empty(size, dtype=int)
        self.board = np.zeros([size, size], dtype=int)
        self.permutations = None

    def check_row(self, board, row_number):
        num_left = 0
        num_right = 0
        highest_left = 0
        highest_right = 0
        for i in range(self.size):
            if board[row_number, i] > highest_left:
                highest_left = board[row_number, i]
                num_left += 1
            if board[row_number, self.size-i-1] > highest_right:
                highest_right = board[row_number, self.size-i-1]
                num_right += 1
        return num_right >= self.right[row_number] and num_left >= self.left[row_number]

    def check_col(self, board, col_number):
        num_top = 0
        num_bottom = 0
        highest_top = 0
        highest_bottom = 0
        zeros = 0
        for i in range(self.size):
            if board[i, col_number] > highest_top:
                highest_top = board[i, col_number]
                num_top += 1
            elif 0 == board[i, col_number] and highest_top < self.size:
                highest_top += 1
                num_top += 1

            if board[self.size-i-1, col_number] > highest_bottom:
                highest_bottom = board[self.size-i-1, col_number]
                num_bottom += 1
            elif 0 == board[self.size-i-1, col_number] and highest_bottom < self.size:
                highest_bottom += 1
                num_bottom += 1
        return (num_top >= self.top[col_number] and num_bottom >= self.bottom[col_number])

    def check_row_unique(self, board, row_number):
        urow = set(board[row_number])
        urow.discard(0)
        return len(urow) == np.count_nonzero(board[row_number])

    def check_col_unique(self, board, col_number, row):
        ucol = set(board[:, col_number])
        ucol.discard(0)  
        return len(ucol) == row+1  

    def backtracking(self, heurestic):
        found_solutions = 0
        recurrences = 0
        nodes = 0

        def more_builds_visible_heurestic():
            constraints = np.zeros(self.size)

            for i in range(self.size):
                constraints[i] = -max(self.left[i], self.right[i])

            return np.argsort(constraints)

        def less_builds_visible_heurestic():
            constraints = np.zeros(self.size)

            for i in range(self.size):
                constraints[i] = max(self.left[i], self.right[i])

            return np.argsort(constraints)

        def check_constraints(board, row, size, row_i):
            if not self.check_row(board, row):
                return False
            for i in range(size):
                if not self.check_col(board, i) or not self.check_col_unique(board, i, row_i):
                    return False
            return True

        def backtracking_recursive(b, row, order):
            nonlocal recurrences
            nonlocal found_solutions
            nonlocal nodes
            if row >= self.size:
                found_solutions += 1
                return
            current = np.copy(b)
            real_row = order[row]
            for p in self.permutations:
                current[real_row] = p
                nodes += 1
                if check_constraints(current, real_row, self.size, row):
                    backtracking_recursive(current, row+1, order)
                else:
                    recurrences += 1

        self.permutations = np.array(
            [p for p in permutations(range(1, self.size+1))])

        def funH(): return range(self.size)
        if "more_build" == heurestic:
            funH = more_builds_visible_heurestic
        elif "less_build" == heurestic:
            funH = less_builds_visible_heurestic

        order_of_search = funH()

        start = time.time()
        backtracking_recursive(self.board, 0, order_of_search)
        stop = time.time()
        return (found_solutions, nodes, recurrences, stop-start)

    def forwardtracking(self, heurestic):
        found_solutions = 0
        recurrences = 0
        nodes = 0

        def more_builds_visible_heurestic():
            constraints = np.zeros(self.size)

            for i in range(self.size):
                constraints[i] = -max(self.left[i], self.right[i])

            return np.argsort(constraints)

        def less_builds_visible_heurestic():
            constraints = np.zeros(self.size)

            for i in range(self.size):
                constraints[i] = max(self.left[i], self.right[i])

            return np.argsort(constraints)

        def check_constraints(board, row, size, row_i):
            if not self.check_row(board, row):
                return False
            for i in range(size):
                if not self.check_col(board, i) or not self.check_col_unique(board, i, row_i):
                    return False
            return True

        def forwardtracking_recursive(b, row, order, ap):
            nonlocal recurrences
            nonlocal found_solutions
            nonlocal nodes
            if row >= self.size:
                found_solutions += 1
                return
            current = np.copy(b)
            real_row = order[row]
            for p in ap:
                current[real_row] = self.permutations[p]
                nodes += 1
                if check_constraints(current, real_row, self.size, row):
                    res = ap - \
                        set(np.argwhere(np.any(self.permutations -
                                               self.permutations[p] == 0, axis=1)).flatten())
                    forwardtracking_recursive(
                        current, row+1, order, res)
                else:
                    recurrences += 1

        self.permutations = np.array(
            [p for p in permutations(range(1, self.size+1))])
        array_of_permutations = set(
            [p for p in range(len(self.permutations))])

        def funH(): return range(self.size)
        if "more_build" == heurestic:
            funH = more_builds_visible_heurestic
        elif "less_build" == heurestic:
            funH = less_builds_visible_heurestic

        order_of_search = funH()
        start = time.time()
        forwardtracking_recursive(
            self.board, 0, order_of_search, array_of_permutations)
        stop = time.time()
        return (found_solutions, nodes, recurrences, stop-start)
