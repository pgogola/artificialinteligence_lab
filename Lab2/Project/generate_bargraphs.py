import math
import numpy as np
import pickle


number = 3

b = None
with open("result_skyscrapper"+str(number)+".txt", "rb") as fp:  # Unpickling
    b = pickle.load(fp)


sky_files = [
    "test_sky_4_0.txt",
    "test_sky_4_1.txt",
    "test_sky_4_2.txt",
    "test_sky_4_3.txt",
    "test_sky_4_4.txt",
    "test_sky_5_0.txt",
    "test_sky_5_1.txt",
    "test_sky_5_2.txt",
    "test_sky_5_3.txt",
    "test_sky_5_4.txt",
    "test_sky_6_0.txt",
    "test_sky_6_1.txt",
    "test_sky_6_2.txt",
    "test_sky_6_3.txt",
    "test_sky_6_4.txt"
]


print("\\begin{figure}[H]")
print("\\centering")
print("\\begin{tikzpicture}")
print("\\begin{axis}[")
print("    ybar,")
print("    enlargelimits = 0.15,")
print("    width = 15cm, height = 10cm,")
print("    legend style = {at = {(0.5, -0.15)},")
print("                    anchor = north, legend columns = 0},")
print("    ylabel = {Czas wykonania (sec)},")
print("    xlabel = {Plik z danymi},")
print("    symbolic x coords = {")
for file_name in sky_files:
    print(file_name.replace("test_sky_", "").replace("_", "\\_") + ", ", end="")
print("},\n    xtick = data,")
print("    nodes near coords,")
print("    nodes near coords align = {vertical},")
print("    title style={at={(0.5,1.0)},anchor=south,yshift=10.1},")
print(
    "    title = {\\textbf{Czas wykonania (sec) dla problemu skyscraper w zależności od użytego Algorytmu}}")
print("]")
for f in [True, False]:
    for heur in ["more_build", "less_build"]:
        times = "\\addplot coordinates {"
        for file_name in sky_files:
            for result in b:
                if result["filename"] == file_name and result["heur"] == heur and result["forward"] == f:
                    times += "(" + file_name.replace("test_sky_",
                                                     "").replace("_", "\\_") + ", "
                    times += "{:.4f}".format(
                        np.mean(result["result"][3])) + ") "
        print(times + "};")
print("\legend{Alg1, Alg2, Alg3, Alg4}")
print("\\end{axis}")
print("\\end{tikzpicture}")
print(
    "\\caption{Czas wykonania (sec) dla problemu skyscraper w zależności od użytego algorytmu}")
print("\end{figure}\n\n\n")


print("\\begin{figure}[H]")
print("\\centering")
print("\\begin{tikzpicture}")
print("\\begin{axis}[")
print("    ybar,")
print("    enlargelimits = 0.15,")
print("    width = 15cm, height = 10cm,")
print("    legend style = {at = {(0.5, -0.15)},")
print("                    anchor = north, legend columns = 0},")
print("    ylabel = {Liczba \\enquote{odwiedzonych} wezlow},")
print("    xlabel = {Plik z danymi},")
print("    symbolic x coords = {")
for file_name in sky_files:
    print(file_name.replace("test_sky_", "").replace("_", "\\_") + ", ", end="")
print("},\n    xtick = data,")
print("    nodes near coords,")
print("    nodes near coords align = {vertical},")
print("    title style={at={(0.5,1.0)},anchor=south,yshift=10.1},")
print(
    "	title = {\\textbf{Liczba \enquote{odwiedzonych} węzłów dla problemu skyscraper w zależności od użytego algorytmu}}")
print("]")
for f in [True, False]:
    for heur in ["more_build", "less_build"]:
        nodes = "\\addplot coordinates {"
        for file_name in sky_files:
            for result in b:
                if result["filename"] == file_name and result["heur"] == heur and result["forward"] == f:
                    nodes += "(" + file_name.replace("test_sky_",
                                                     "").replace("_", "\\_") + ", "
                    nodes += str(np.max(result["result"][1])) + ") "
        print(nodes + "};")
print("\legend{Alg1, Alg2, Alg3, Alg4}")
print("\\end{axis}")
print("\\end{tikzpicture}")
print(
    "\\caption{Liczba \enquote{odwiedzonych} węzłów dla problemu skyscraper w zależności od użytego algorytmu}")
print("\end{figure}\n\n\n")

print("\\begin{figure}[H]")
print("\\centering")
print("\\begin{tikzpicture}")
print("\\begin{axis}[")
print("    ybar,")
print("    enlargelimits = 0.15,")
print("    width = 15cm, height = 10cm,")
print("    legend style = {at = {(0.5, -0.15)},")
print("                    anchor = north, legend columns = 0},")
print("    ylabel = {Liczba \\enquote{nawrotow}},")
print("    xlabel = {Plik z danymi},")
print("    symbolic x coords = {")
for file_name in sky_files:
    print(file_name.replace("test_sky_", "").replace("_", "\\_") + ", ", end="")
print("},\n    xtick = data,")
print("    nodes near coords,")
print("    nodes near coords align = {vertical},")
print("    title style={at={(0.5,1.0)},anchor=south,yshift=10.1},")
print(
    "	title = {\\textbf{Liczba \enquote{nawrotów} dla problemu skyscraper w zależności od użytego algorytmu}}")
print("]")
for f in [True, False]:
    for heur in ["more_build", "less_build"]:
        recurences = "\\addplot coordinates {"
        for file_name in sky_files:
            for result in b:
                if result["filename"] == file_name and result["heur"] == heur and result["forward"] == f:
                    recurences += "(" + file_name.replace("test_sky_",
                                                          "").replace("_", "\\_") + ", "
                    recurences += str(np.max(result["result"][2])) + ") "
        print(recurences + "};")
print("\legend{Alg1, Alg2, Alg3, Alg4}")
print("\\end{axis}")
print("\\end{tikzpicture}")
print(
    "\\caption{Liczba \enquote{nawrotów} dla problemu skyscraper w zależności od użytego algorytmu}")
print("\end{figure}\n\n\n")


b = None
with open("result_futoshiki"+str(number)+".txt", "rb") as fp:    # Unpickling
    b = pickle.load(fp)

futoshiki_files = [
    "test_futo_4_0.txt",
    "test_futo_4_1.txt",
    "test_futo_4_2.txt",
    "test_futo_5_0.txt",
    "test_futo_5_1.txt",
    "test_futo_5_2.txt",
    "test_futo_6_0.txt",
    "test_futo_6_1.txt",
    "test_futo_6_2.txt",
    "test_futo_7_0.txt",
    "test_futo_7_1.txt",
    "test_futo_7_2.txt",
    "test_futo_8_0.txt",
    "test_futo_8_1.txt",
    "test_futo_8_2.txt",
    "test_futo_9_0.txt",
    "test_futo_9_1.txt",
    "test_futo_9_2.txt"
]

print("\\begin{figure}[H]")
print("\\centering")
print("\\begin{tikzpicture}")
print("\\begin{axis}[")
print("    ybar,")
print("    enlargelimits = 0.15,")
print("    width = 15cm, height = 10cm,")
print("    legend style = {at = {(0.5, -0.15)},")
print("                    anchor = north, legend columns = 0},")
print("    ylabel = {Czas wykonania (sec)},")
print("    xlabel = {Plik z danymi},")
print("    symbolic x coords = {")
for file_name in futoshiki_files:
    print(file_name.replace("test_futo_", "").replace("_", "\\_") + ", ", end="")
print("},\n    xtick = data,")
print("    nodes near coords,")
print("    nodes near coords align = {vertical},")
print("    title style={at={(0.5,1.0)},anchor=south,yshift=10.1},")
print(
    "    title = {\\textbf{Czas wykonania (sec) dla problemu futoshiki w zależności od użytego algorytmu}}")
print("]")
for f in [True, False]:
    for heur in ["biggest_con", "lowest_con"]:
        times = "\\addplot coordinates {"
        for file_name in futoshiki_files:
            for result in b:
                if result["filename"] == file_name and result["heur"] == heur and result["forward"] == f:
                    times += "(" + file_name.replace("test_futo_",
                                                     "").replace("_", "\\_") + ", "
                    times += "{:.4f}".format(
                        np.mean(result["result"][3])) + ") "
        print(times + "};")
print("\legend{Alg1, Alg2, Alg3, Alg4}")
print("\\end{axis}")
print("\\end{tikzpicture}")
print(
    "\\caption{Czas wykonania (sec) dla problemu futoshiki w zależności od użytego algorytmu}")
print("\end{figure}\n\n\n")


print("\\begin{figure}[H]")
print("\\centering")
print("\\caption{Liczba \\enquote{odwiedzonych} węzłów}")
print("\\begin{tikzpicture}")
print("\\begin{axis}[")
print("    ybar,")
print("    enlargelimits = 0.15,")
print("    width = 15cm, height = 10cm,")
print("    legend style = {at = {(0.5, -0.15)},")
print("                    anchor = north, legend columns = 0},")
print("    ylabel = {Liczba \\enquote{odwiedzonych} wezlow},")
print("    xlabel = {Plik z danymi},")
print("    symbolic x coords = {")
for file_name in futoshiki_files:
    print(file_name.replace("test_futo_", "").replace("_", "\\_") + ", ", end="")
print("},\n    xtick = data,")
print("    nodes near coords,")
print("    nodes near coords align = {vertical},")
print("    title style={at={(0.5,1.0)},anchor=south,yshift=10.1},")
print(
    "	title = {\\textbf{Liczba \enquote{odwiedzonych} węzłów dla problemu futoshiki w zależności od użytego algorytmu}}")
print("]")
for f in [True, False]:
    for heur in ["biggest_con", "lowest_con"]:
        nodes = "\\addplot coordinates {"
        for file_name in futoshiki_files:
            for result in b:
                if result["filename"] == file_name and result["heur"] == heur and result["forward"] == f:
                    nodes += "(" + file_name.replace("test_futo_",
                                                     "").replace("_", "\\_") + ", "
                    nodes += str(np.max(result["result"][1])) + ") "
        print(nodes + "};")
print("\legend{Alg1, Alg2, Alg3, Alg4}")
print("\\end{axis}")
print("\\end{tikzpicture}")
print(
    "\\caption{Liczba \enquote{odwiedzonych} węzłów dla problemu futoshiki w zależności od użytego algorytmu}")
print("\end{figure}\n\n\n")

print("\\begin{figure}[H]")
print("\\centering")
print("\\caption{Liczba \\enquote{nawrotów}}")
print("\\begin{tikzpicture}")
print("\\begin{axis}[")
print("    ybar,")
print("    enlargelimits = 0.15,")
print("    width = 15cm, height = 10cm,")
print("    legend style = {at = {(0.5, -0.15)},")
print("                    anchor = north, legend columns = 0},")
print("    ylabel = {Liczba \\enquote{nawrotow}},")
print("    xlabel = {Plik z danymi},")
print("    symbolic x coords = {")
for file_name in futoshiki_files:
    print(file_name.replace("test_futo_", "").replace("_", "\\_") + ", ", end="")
print("},\n    xtick = data,")
print("    nodes near coords,")
print("    nodes near coords align = {vertical},")
print("    title style={at={(0.5,1.0)},anchor=south,yshift=10.1},")
print(
    "	title = {\\textbf{Liczba \enquote{nawrotów} dla problemu futoshiki w zależności od użytego algorytmu}}")
print("]")
for f in [True, False]:
    for heur in ["biggest_con", "lowest_con"]:
        recurences = "\\addplot coordinates {"
        for file_name in futoshiki_files:
            for result in b:
                if result["filename"] == file_name and result["heur"] == heur and result["forward"] == f:
                    recurences += "(" + file_name.replace("test_futo_",
                                                          "").replace("_", "\\_") + ", "
                    recurences += str(np.max(result["result"][2])) + ") "
        print(recurences + "};")
print("\legend{Alg1, Alg2, Alg3, Alg4}")
print("\\end{axis}")
print("\\end{tikzpicture}")
print(
    "\\caption{Liczba \enquote{nawrotów} dla problemu skyscraper w zależności od użytego algorytmu}")
print("\end{figure}\n\n\n")
