import data_loader as dl
import futoshiki
import skyscrapper
from itertools import permutations
import time
import numpy as np
import tester
import pickle
import sys
import threading


def main(arg, oper):
    futoshiki_files_tab = [
        ["test_futo_4_0.txt",
         "test_futo_4_1.txt",
         "test_futo_4_2.txt"],
        ["test_futo_5_0.txt",
         "test_futo_5_1.txt",
         "test_futo_5_2.txt"],
        ["test_futo_6_0.txt",
         "test_futo_6_1.txt",
         "test_futo_6_2.txt"],
        ["test_futo_7_0.txt",
         "test_futo_7_1.txt",
         "test_futo_7_2.txt"],
        ["test_futo_8_0.txt",
         "test_futo_8_1.txt",
         "test_futo_8_2.txt"],
        ["test_futo_9_0.txt",
         "test_futo_9_1.txt",
         "test_futo_9_2.txt"]
    ]

    sky_files_tab = [
        ["test_sky_4_0.txt",
         "test_sky_4_1.txt",
         "test_sky_4_2.txt",
         "test_sky_4_3.txt",
         "test_sky_4_4.txt"],
        ["test_sky_5_0.txt",
         "test_sky_5_1.txt",
         "test_sky_5_2.txt",
         "test_sky_5_3.txt",
         "test_sky_5_4.txt"],
        ["test_sky_6_0.txt",
         "test_sky_6_1.txt",
         "test_sky_6_2.txt",
         "test_sky_6_3.txt",
         "test_sky_6_4.txt"]
    ]

    futoshiki_heurestics = [
        "biggest_con",
        "lowest_con"
    ]

    skyscrapper_heurestics = [
        "more_build",
        "less_build"
    ]

    test_folder_name = "./test_data/"

    rounds_amount = 10

    def thread_functionsky():
        if arg <= len(sky_files_tab):
            sky_files = sky_files_tab[arg-1]
            results = []
            for fn in sky_files:
                file_name = fn
                for f in range(2):
                    for heurestic in skyscrapper_heurestics:
                        result = tester.runTest(file_name=test_folder_name+file_name,
                                                rounds=rounds_amount, used_heurestic=heurestic, problem_type="skyscrapper", forward=(f % 2 == 0))
                        print(file_name)
                        obj = {"filename": file_name, "heur": heurestic,
                               "forward": (f % 2 == 0), "result": result}
                        results.append(obj)

            with open("result_skyscrapper"+str(arg)+".txt", "wb") as fp:
                pickle.dump(results, fp)

    def thread_functionfuto():
        if arg <= len(futoshiki_files_tab):
            futoshiki_files = futoshiki_files_tab[arg-1]
            results = []
            for fn in futoshiki_files:
                file_name = fn
                for f in range(2):
                    for heurestic in futoshiki_heurestics:
                        result = tester.runTest(file_name=test_folder_name+file_name,
                                                rounds=rounds_amount, used_heurestic=heurestic, problem_type="futoshiki", forward=(f % 2 == 0))
                        print(file_name)
                        obj = {"filename": file_name, "heur": heurestic,
                               "forward": (f % 2 == 0), "result": result}
                        results.append(obj)

            with open("result_futoshiki"+str(arg)+".txt", "wb") as fp:
                pickle.dump(results, fp)

    if oper == 1:
        thread_functionsky()
    if oper == 2:
        thread_functionfuto()


if __name__ == "__main__":
    main(int(sys.argv[1]), int(sys.argv[2]))