from others import eprint
import numpy as np
import cv2


class ImageMatcher:
    def __init__(self, image1, image2):
        self.image1 = image1
        self.image2 = image2
        self.distances = None
        self.points_pairs = None
        self.Q = dict()
        self.P = dict()
        self.all_points = set()
        self.after_ramsac = set()

    def extract_images_points(self):
        self.image1.extract_characteristic_points()
        self.image2.extract_characteristic_points()

    def load_images_points(self):
        self.image1.load_characteristic_points()
        self.image2.load_characteristic_points()

    def distance_matrix(self):
        eprint("Calculating distance matrix for images " +
               self.image1.image_name + " and " + self.image2.image_name)
        '''
            distance matrix [image1_points, image2_points]
            rows are points from first image
            columns are points from second image
        '''
        img1_points_len = len(self.image1.characteristic_points)
        img2_points_len = len(self.image2.characteristic_points)
        self.distances = np.zeros((img1_points_len, img2_points_len))
        for img1_idx in range(img1_points_len):
            for img2_idx in range(img2_points_len):
                self.distances[img1_idx, img2_idx] = np.sum(np.subtract(
                    self.image1.characteristic_points[img1_idx].feature_vector, self.image2.characteristic_points[img2_idx].feature_vector)**2)

    def indices_of_paired_points(self, distances):
        eprint("Calculating indices points for images " +
               self.image1.image_name + " and " + self.image2.image_name)
        mins_in_col = np.min(distances, axis=0)   # min value in column
        mins_in_row = np.min(distances, axis=1)
        self.points_pairs = set()
        self.P = dict()
        self.Q = dict()
        for i in range(len(mins_in_row)):
            for j in range(len(mins_in_col)):
                if mins_in_col[j] == mins_in_row[i] and distances[i, j] == mins_in_row[i]:
                    self.points_pairs.add((i, j))
                    if not i in self.P:
                        self.P[i] = set()
                    if not j in self.Q:
                        self.Q[j] = set()
                    self.P[i].add((i, j))
                    self.Q[j].add((i, j))
        self.all_points = self.points_pairs.copy()
        eprint("Found " + str(len(self.points_pairs)) + " points.")

    def points_in_neighborhood(self, k, p):
        eprint("Calculating indices points in neighborhood for images " +
               self.image1.image_name + " and " + self.image2.image_name)

        first_image_indices = list(self.P.keys())
        second_image_indices = list(self.Q.keys())

        img1_distances = np.zeros(
            (len(first_image_indices), len(first_image_indices)))
        img2_distances = np.zeros(
            (len(second_image_indices), len(second_image_indices)))

        for i in range(len(first_image_indices)):
            for j in range(i, len(first_image_indices)):
                if i == j:  # self.image1.characteristic_points[first_image_indices[i]].coordinates == \
                        # self.image1.characteristic_points[first_image_indices[j]].coordinates:
                    img1_distances[i, j] = img1_distances[j, i] = float('inf')
                else:
                    img1_distances[j, i] = img1_distances[i, j] = (
                        self.image1.characteristic_points[first_image_indices[i]].coordinates[0] -
                        self.image1.characteristic_points[first_image_indices[j]].coordinates[0])**2 +\
                        (self.image1.characteristic_points[first_image_indices[i]].coordinates[1] -
                         self.image1.characteristic_points[first_image_indices[j]].coordinates[1])**2
        for i in range(len(second_image_indices)):
            for j in range(i, len(second_image_indices)):
                if i == j:  # self.image2.characteristic_points[second_image_indices[i]].coordinates == \
                        # self.image2.characteristic_points[second_image_indices[j]].coordinates:
                    img2_distances[i, j] = img2_distances[j, i] = float('inf')
                else:
                    img2_distances[j, i] = img2_distances[i, j] = (
                        self.image2.characteristic_points[second_image_indices[i]].coordinates[0] -
                        self.image2.characteristic_points[second_image_indices[j]].coordinates[0])**2 +\
                        (self.image2.characteristic_points[second_image_indices[i]].coordinates[1] -
                         self.image2.characteristic_points[second_image_indices[j]].coordinates[1])**2
        neighborhood_img1 = np.transpose(
            np.argsort(img1_distances, axis=0)[:k])
        neighborhood_img2 = np.transpose(
            np.argsort(img2_distances, axis=0)[:k])

        P_pairset = dict()
        Q_pairset = dict()
        for i in range(len(first_image_indices)):
            # print("P")
            # print(
            #     self.image1.characteristic_points[first_image_indices[i]].coordinates)
            P_pairset[first_image_indices[i]] = set()
            for j in neighborhood_img1[i]:
                P_pairset[first_image_indices[i]
                          ] |= self.P[first_image_indices[j]]
            # print(P_pairset[first_image_indices[i]])
            # input()
        for i in range(len(second_image_indices)):
            # print("Q")
            # print(
            #     self.image2.characteristic_points[second_image_indices[i]].coordinates)
            Q_pairset[second_image_indices[i]] = set()
            for j in neighborhood_img2[i]:
                Q_pairset[second_image_indices[i]
                          ] |= self.Q[second_image_indices[j]]
            # print(Q_pairset[second_image_indices[i]])
            # input()
        left_points = set()
        for elem in self.points_pairs:
            # print(len(P_pairset[elem[0]] & Q_pairset[elem[1]])/k)
            if len(P_pairset[elem[0]] & Q_pairset[elem[1]])/k >= p:
                left_points.add(elem)

        self.points_pairs = left_points
        eprint("Left " + str(len(self.points_pairs)) + " points.")

    def concatenate_images(self, horizontal=True):
        img1 = self.image1.image
        img2 = self.image2.image

        if horizontal:
            max_h = max([img1.shape[0], img2.shape[0]])
            image_res1 = np.zeros(
                (max_h, img1.shape[1], img1.shape[2]), dtype="uint8")
            image_res1[0:img1.shape[0], 0:img1.shape[1], :] = img1
            image_res2 = np.zeros(
                (max_h, img2.shape[1], img2.shape[2]), dtype="uint8")
            image_res2[:img2.shape[0], :img2.shape[1], :] = img2
            return np.hstack((image_res1, image_res2))

        max_w = max([img1.shape[1], img2.shape[1]])
        image_res1 = np.zeros(
            (img1.shape[0], max_w, img1.shape[2]), dtype="uint8")
        image_res1[0:img1.shape[0], 0:img1.shape[1], :] = img1
        image_res2 = np.zeros(
            (img2.shape[0], max_w, img2.shape[2]), dtype="uint8")
        image_res2[:img2.shape[0], :img2.shape[1], :] = img2
        return np.vstack((image_res1, image_res2))

    def draw_matched_points(self, color=(0, 255, 255), horizontal=True):
        concatenated = self.concatenate_images(horizontal)
        if horizontal:
            for p in self.all_points:
                cv2.circle(
                    concatenated, self.image1.characteristic_points[p[0]].coordinates, 2, (0, 255, 0), -1)
                cv2.circle(concatenated, (self.image1.image.shape[1] + self.image2.characteristic_points[p[1]].coordinates[0],
                                          self.image2.characteristic_points[p[1]].coordinates[1]), 2, (0, 255, ), -1)
            for p in self.after_ramsac:
                cv2.circle(
                    concatenated, self.image1.characteristic_points[p[0]].coordinates, 2, (0, 0, 255), -1)
                cv2.circle(concatenated, (self.image1.image.shape[1] + self.image2.characteristic_points[p[1]].coordinates[0],
                                          self.image2.characteristic_points[p[1]].coordinates[1]), 2, (0, 0, 255), -1)
            for p in self.points_pairs:
                # print(
                #     self.image1.characteristic_points[p[0]].coordinates)
                # print(self.image2.characteristic_points[p[1]].coordinates)
                cv2.line(concatenated, self.image1.characteristic_points[p[0]].coordinates,
                         (self.image1.image.shape[1] + self.image2.characteristic_points[p[1]].coordinates[0],
                          self.image2.characteristic_points[p[1]].coordinates[1]), color, 1)

        else:
            for p in self.points_pairs:
                cv2.line(concatenated, p[0].coordinates,
                         (p[1].coordinates[0], self.image1.image.shape[0] + p[1].coordinates[1]), color, 1)

        return concatenated

    def filter_points_by_model(self, model, delta):
        eprint("Calculating ramsac filtering for images " +
               self.image1.image_name + " and " + self.image2.image_name)

        def transform_point(point, model):
            v = np.matmul(model, [point.coordinates[0],
                                  point.coordinates[1], 1])
            return v[0:2]/v[2]

        def distance_between_points(point_a, point_b):
            return (point_a.coordinates[0]-point_b[0])**2+(point_a.coordinates[1]-point_b[1])**2

        result_points = []
        delta_pow = delta**2
        for pair in self.points_pairs:
            if distance_between_points(self.image2.characteristic_points[pair[1]], transform_point(self.image1.characteristic_points[pair[0]], model)) <= delta_pow:
                result_points.append(pair)

        self.points_pairs = result_points
        self.after_ramsac = self.points_pairs.copy()

        eprint("Left " + str(len(self.points_pairs)) + " points.")
