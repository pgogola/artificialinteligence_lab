from task_wrapper import task_wrapper
import cv2
from ramsac import ramsac_iterations_calculation


def main():

    print(ramsac_iterations_calculation(0.8, 0.2, 4))
    images = [  # budynki
        ["katedra1.png", "katedra2.png"],
        ["photo21.png", "photo22.png"],
        ["photo11.png", "photo12.png"],
        ["photo31.png", "photo32.png"],
        ["amerykanka.png", "amerykanka2.png"],
        ["ratusz4.png", "ratusz1.png"],
        ["pomnik.png", "pomnik2.png"],
        ["jozef.png", "jozef2.png"],
        ["szpital.png", "szpital2.png"],
        ["cukrownia.png", "cukrownia2.png"],
        # inne
        ["kaczuszka31.png", "kaczuszka32.png"],
        ["ksiazka10.png", "ksiazka22.png"],
        ["klucze11.png", "klucze12.png"],
        ["ksiazka10.png", "ksiazka12.png"],
        ["kaczuszka41.png", "kaczuszkax.png"]]

    transformations = ['affine', 'perspective']

    # parameters:
    for selected_images in images:
        ramsac_first = True
        # selected_images = images[13]
        transformation = transformations[1]
        rammsac_iterations = 1000
        max_ramsac_error = 20
        ramsac_heurestic = "RandomPointsSimpleRandom"
        neighborhood_p = 0.5
        neighborhood_k = 10
        ramsac_filter_threshold = 40

        cv2.imshow("aaa", task_wrapper(ramsac_first,
                                       selected_images,
                                       transformation,
                                       rammsac_iterations,
                                       max_ramsac_error,
                                       ramsac_heurestic,
                                       neighborhood_p=neighborhood_p,
                                       neighborhood_k=neighborhood_k,
                                       ramsac_filter_threshold=ramsac_filter_threshold, r=0, R=30).result_image)
        cv2.waitKey()


if __name__ == "__main__":
    main()
