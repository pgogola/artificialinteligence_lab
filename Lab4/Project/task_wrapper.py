
import time
import pickle
import cv2
import os
import numpy as np
from Image import Image
from ImageMatcher import ImageMatcher
from ramsac import *
import math


class ResultWrapper:
    def __init__(self,
                 image1_name,
                 image2_name,
                 found_points,
                 result_image,
                 ramsac_first,
                 selected_images,
                 transformation,
                 rammsac_iterations,
                 max_ramsac_error,
                 ramsac_heurestic,
                 neighborhood_p,
                 neighborhood_k,
                 ramsac_filter_threshold,
                 time,
                 r,
                 R,
                 p,
                 w):
        self.found_points = found_points
        self.result_image = result_image
        self.ramsac_first = ramsac_first
        self.selected_images = selected_images
        self.transformation = transformation
        self.rammsac_iterations = rammsac_iterations
        self.max_ramsac_error = max_ramsac_error
        self.ramsac_heurestic = ramsac_heurestic
        self.neighborhood_p = neighborhood_p
        self.neighborhood_k = neighborhood_k
        self.ramsac_filter_threshold = ramsac_filter_threshold
        self.time = time
        self.image1_name = image1_name
        self.image2_name = image2_name
        self.R = R
        self.r = r
        self.p = p
        self.w = w


def task_wrapper(ramsac_first,
                 selected_images,
                 transformation,
                 rammsac_iterations,
                 max_ramsac_error,
                 ramsac_heurestic,
                 neighborhood_p,
                 neighborhood_k,
                 ramsac_filter_threshold,
                 r=None,
                 R=None,
                 p=None,
                 w=None):
    distances_file_name = "./distances/distances_" + \
        selected_images[0].replace(
            ".png", "_")+selected_images[1].replace(".png", "") + ".pickle"

    image_name1 = "./images/"+selected_images[0]
    image_name2 = "./images/"+selected_images[1]
    image1 = Image(image_name1)
    image2 = Image(image_name2)
    image_matcher = ImageMatcher(image1, image2)

    if not os.path.isfile(image_name1+".haraff.sift") \
            or not os.path.isfile(image_name2+".haraff.sift"):
        image_matcher.extract_images_points()
    image_matcher.load_images_points()

    exists_distance_file = os.path.isfile(distances_file_name)
    if not exists_distance_file:
        image_matcher.distance_matrix()
        distances = image_matcher.distances
        with open(distances_file_name, 'wb') as df:
            pickle.dump(distances, df)
    else:
        with open(distances_file_name, 'rb') as df:
            distances = pickle.load(df)

    start = time.time()
    image_matcher.indices_of_paired_points(distances)

    ramsac_heurestic_class = None
    ramsac_points = 4 if transformation == "perspective" else 3
    if ramsac_heurestic == "RandomPointsSimpleRandom":
        ramsac_heurestic_class = RandomPointsSimpleRandom(None, ramsac_points)
    elif ramsac_heurestic == "RandomPointsRr":
        im1size = min(image1.image.shape[0], image1.image.shape[1])
        im2size = min(image2.image.shape[0], image2.image.shape[1])
        ramsac_heurestic_class = RandomPointsRr(
            None, ramsac_points, im1size*R/100,  im1size*r/100+1,  im2size*R/100,  im2size*r/100+1)
    elif ramsac_heurestic == "RandomPointsProbabilityRr":
        im1size = min(image1.image.shape[0], image1.image.shape[1])
        im2size = min(image2.image.shape[0], image2.image.shape[1])
        ramsac_heurestic_class = RandomPointsProbabilityRr(
            None, ramsac_points, im1size*3/10,  1,  im2size*3/10,  1)
    elif ramsac_heurestic == "RandomPointsPreviousBest":
        im1size = min(image1.image.shape[0], image1.image.shape[1])
        im2size = min(image2.image.shape[0], image2.image.shape[1])
        ramsac_heurestic_class = RandomPointsPreviousBest(
            None, ramsac_points, im1size*3/10,  1,  im2size*3/10,  1)
    elif ramsac_heurestic == "RandomPointsProbability":
        ramsac_heurestic_class = RandomPointsProbability(None, ramsac_points)

    if ramsac_first:
        pairs_indices = image_matcher.points_pairs
        pairs = [[image1.characteristic_points[x[0]],
                  image2.characteristic_points[x[1]]] for x in pairs_indices]
        ramsac_heurestic_class.data = pairs
        ramsac_heurestic_class.init()
        transformation_model = ramsac_method(
            data=pairs, iterations=rammsac_iterations, maxerror=max_ramsac_error,
            random_class=ramsac_heurestic_class, transformation=transformation)
        image_matcher.filter_points_by_model(
            transformation_model, ramsac_filter_threshold)
        image_matcher.points_in_neighborhood(
            k=math.ceil(neighborhood_k*len(image_matcher.points_pairs)/100), p=neighborhood_p)
    else:
        image_matcher.points_in_neighborhood(
            k=math.ceil(neighborhood_k*len(image_matcher.points_pairs)/100), p=neighborhood_p)
        pairs_indices = image_matcher.points_pairs
        if len(pairs_indices) > ramsac_points:
            pairs = [[image1.characteristic_points[x[0]],
                      image2.characteristic_points[x[1]]] for x in pairs_indices]
            ramsac_heurestic_class.data = pairs
            ramsac_heurestic_class.init()
            transformation_model = ramsac_method(
                data=pairs, iterations=rammsac_iterations, maxerror=max_ramsac_error,
                random_class=ramsac_heurestic_class, transformation=transformation)
            image_matcher.filter_points_by_model(
                transformation_model, ramsac_filter_threshold)
    stop = time.time()
    proces_time = stop-start
    merged_result = image_matcher.draw_matched_points(horizontal=True)
    return ResultWrapper(
        image1_name=selected_images[0],
        image2_name=selected_images[1],
        found_points=len(image_matcher.points_pairs),
        result_image=merged_result,
        ramsac_first=ramsac_first,
        selected_images=selected_images,
        transformation="Afiniczna" if transformation == "affine" else "Perspektywiczna",
        rammsac_iterations=rammsac_iterations,
        max_ramsac_error=max_ramsac_error,
        ramsac_heurestic=ramsac_heurestic,
        neighborhood_p=neighborhood_p,
        neighborhood_k=neighborhood_k,
        ramsac_filter_threshold=ramsac_filter_threshold,
        time=proces_time,
        r=r,
        R=R,
        p=p,
        w=w)
