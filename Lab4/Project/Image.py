import cv2
import numpy as np
from others import eprint
from CharacteristicPoint import CharacteristicPoint
import os


class Image:
    def __init__(self, image_name):
        self.image_name = image_name
        self.image = cv2.imread(self.image_name)
        self.characteristic_points = []

    def extract_characteristic_points(self, draw=False):
        eprint("Extracting characteristic points from image " + self.image_name)
        '''
            we wspolrzednych pierwsza wartosc odpowiada kolumnie, druga wierszowi
        '''
        feature_extractor = "./extract_features/extract_features_32bit.ln -haraff -sift -i " + \
            self.image_name + (" -DE" if draw else "")
        os.system(feature_extractor)

    def load_characteristic_points(self):
        eprint("Loading characteristic points for image " + self.image_name)
        feature_vectors_file_name = self.image_name + ".haraff.sift"
        with open(feature_vectors_file_name, 'r') as features:
            for vector in features:
                values = vector.split()
                if len(values) < 3:
                    continue
                coordinates = (int(float(values[0])), int(float(values[1])))
                self.characteristic_points.append(
                    CharacteristicPoint(coordinates, np.asarray(values[5:], dtype=np.float16)/255))
