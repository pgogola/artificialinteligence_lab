from __future__ import print_function
import sys


def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)


def create_file_name(result):
    result_filename = f"{result.image1_name}" +\
        f"{result.image2_name}" +\
        f"fp{result.found_points}" +\
        f"{result.transformation}" +\
        f"it{result.rammsac_iterations}" +\
        f"{result.ramsac_heurestic}" + \
        (f"r={result.r}R={result.R}" if result.ramsac_heurestic ==
         "RandomPointsRr" else "") +\
        f"mre{result.max_ramsac_error}" +\
        f"rft{result.ramsac_filter_threshold}" + \
        f"nk{result.neighborhood_k}" +\
        f"np{result.neighborhood_p}" +\
        f"p{result.p}" +\
        f"w{result.w}"

    return result_filename.replace(".", "").replace("=", "")
