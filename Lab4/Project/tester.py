from task_wrapper import task_wrapper
from ramsac import *
import pickle
import cv2
from Image import Image
from ImageMatcher import ImageMatcher
import os
from others import *


def save_data(data, file_name):
    with open("./results/" + file_name + ".dat", "wb") as f:
        pickle.dump(data, f)


def load_data(file_name):
    with open(file_name + ".dat", "rb") as f:
        d = pickle.load(f)
    return d


def tester():
    photos = [
        # budynki
        ["katedra1.png", "katedra2.png"],
        ["photo21.png", "photo22.png"],
        ["photo11.png", "photo12.png"],
        ["photo31.png", "photo32.png"],
        ["amerykanka.png", "amerykanka2.png"],
        ["ratusz4.png", "ratusz1.png"],
        ["pomnik.png", "pomnik2.png"],
        ["jozef.png", "jozef2.png"],
        ["szpital.png", "szpital2.png"],
        ["cukrownia.png", "cukrownia2.png"],
        # inne
        ["kaczuszka31.png", "kaczuszka32.png"],
        ["ksiazka10.png", "ksiazka22.png"],
        ["klucze11.png", "klucze12.png"],
        ["ksiazka10.png", "ksiazka12.png"],
        ["kaczuszka41.png", "kaczuszkax.png"]]

    # for selected_images in photos:
    #     distances_file_name = "./distances/distances_" + \
    #         selected_images[0].replace(
    #             ".png", "_")+selected_images[1].replace(".png", "") + ".pickle"

    #     image_name1 = "./images/"+selected_images[0]
    #     image_name2 = "./images/"+selected_images[1]
    #     image1 = Image(image_name1)
    #     image2 = Image(image_name2)
    #     image_matcher = ImageMatcher(image1, image2)

    #     if not os.path.isfile(image_name1+".haraff.sift") \
    #             or not os.path.isfile(image_name2+".haraff.sift"):
    #         image_matcher.extract_images_points()
    #     image_matcher.load_images_points()

    #     exists_distance_file = os.path.isfile(distances_file_name)
    #     if not exists_distance_file:
    #         image_matcher.distance_matrix()
    #         distances = image_matcher.distances
    #         with open(distances_file_name, 'wb') as df:
    #             pickle.dump(distances, df)
    #     else:
    #         with open(distances_file_name, 'rb') as df:
    #             distances = pickle.load(df)

    ramsac_heurestic_arr = ["RandomPointsSimpleRandom", "RandomPointsRr",
                            "RandomPointsProbability", "RandomPointsProbabilityRr",
                            "RandomPointsPreviousBest"]

    ramsac_transformation_arr = ["affine", "perspective"]

    # neighborhood_p_arr = [0.50, 0.75]
    # neighborhood_k_arr = [5, 10, 15, 20]
    # ramsac_error = [1, 5, 10, 20]
    # ramsac_filter_error = [1, 5, 10, 20]
    # ramsac_iterations_p = [0.4, 0.6, 0.8]
    # ramsac_iterations_w = [0.2, 0.4, 0.6]

    neighborhood_p_arr = [0.40, 0.6, 0.8]
    neighborhood_k_arr = [5, 15, 25]
    ramsac_error = [10, 30, 50]
    ramsac_filter_error = [20, 60, 70]
    ramsac_iterations_p = [0.3, 0.6, 0.9]
    ramsac_iterations_w = [0.4, 0.6, 0.8]

    ramsac_heurestic_default = ["RandomPointsSimpleRandom"]
    ramsac_transformation_arr_default = ["affine"]
    neighborhood_p_arr_default = [0.50]
    neighborhood_k_arr_default = [10]
    ramsac_error_default = [20]
    ramsac_filter_error_default = [40]
    ramsac_iterations_p_default = [0.8]
    ramsac_iterations_w_default = [0.2]

    results = []
    for conf in range(8):
        true_false = np.zeros((9, ), dtype=bool)
        true_false[conf] = True
        for images in photos:
            for n_p in neighborhood_p_arr if true_false[0] else neighborhood_p_arr_default:
                for n_k in neighborhood_k_arr if true_false[1] else neighborhood_k_arr_default:
                    for r_h in ramsac_heurestic_arr if true_false[2] else ramsac_heurestic_default:
                        for r_t in ramsac_transformation_arr if true_false[3] else ramsac_transformation_arr_default:
                            for r_e in ramsac_error if true_false[4] else ramsac_error_default:
                                for r_f_e in ramsac_filter_error if true_false[5] else ramsac_filter_error_default:
                                    for r_i_p in ramsac_iterations_p if true_false[6] else ramsac_iterations_p_default:
                                        for r_i_w in ramsac_iterations_w if true_false[7] else ramsac_iterations_w_default:
                                            if r_h == "RandomPointsRr":
                                                # for r in [0, 3]:
                                                #     for R in [25, 30, 40]:
                                                # if r < R:
                                                    # parameters:
                                                ramsac_first = True
                                                selected_images = images
                                                transformation = r_t
                                                rammsac_iterations = ramsac_iterations_calculation(
                                                    r_i_p, r_i_w, 3 if transformation == "affine" else 4)
                                                max_ramsac_error = r_e
                                                ramsac_heurestic = r_h
                                                neighborhood_p = n_p
                                                neighborhood_k = n_k
                                                ramsac_filter_threshold = r_f_e

                                                result = task_wrapper(ramsac_first,
                                                                      selected_images,
                                                                      transformation,
                                                                      rammsac_iterations,
                                                                      max_ramsac_error,
                                                                      ramsac_heurestic,
                                                                      neighborhood_p,
                                                                      neighborhood_k,
                                                                      ramsac_filter_threshold,
                                                                      r=0,
                                                                      R=30,
                                                                      p=r_i_p,
                                                                      w=r_i_w)
                                                results.append(result)
                                                print("finish")
                                            else:
                                                # parameters:
                                                ramsac_first = True
                                                selected_images = images
                                                transformation = r_t
                                                rammsac_iterations = ramsac_iterations_calculation(
                                                    r_i_p, r_i_w, 3 if transformation == "affine" else 4)
                                                max_ramsac_error = r_e
                                                ramsac_heurestic = r_h
                                                neighborhood_p = n_p
                                                neighborhood_k = n_k
                                                ramsac_filter_threshold = r_f_e

                                                result = task_wrapper(ramsac_first,
                                                                      selected_images,
                                                                      transformation,
                                                                      rammsac_iterations,
                                                                      max_ramsac_error,
                                                                      ramsac_heurestic,
                                                                      neighborhood_p,
                                                                      neighborhood_k,
                                                                      ramsac_filter_threshold,
                                                                      p=r_i_p,
                                                                      w=r_i_w)
                                                results.append(result)
                                                print("finish")
                                            result_filename = create_file_name(
                                                result)
                                            print(result_filename)
                                            cv2.imwrite(
                                                f"./tmpresults/{result_filename}.png", result.result_image)
    # in1=images[0].replace(".", "")
    # in2=images[1].replace(".", "")
    save_data(results, f"wyniki_raw")


def create_results_table(results):
    tables_rows = [
        "Liczba znalezionych par",
        "Transformata",
        "Liczba iteracji",
        "Heur. wyboru pkt",
        "$\\varepsilon$ modelu",
        "$\\Delta{}$",
        "Wielkość sąsiedztwa",
        "Próg akceptacji sąsiedz.",
        "p",
        "w"
    ]

    param_names = [
        "found_points",
        "transformation",
        "rammsac_iterations",
        "ramsac_heurestic",
        "max_ramsac_error",
        "ramsac_filter_threshold",
        "neighborhood_k",
        "neighborhood_p",
        "p",
        "w"
    ]

    heur_to_name = {
        "RandomPointsSimpleRandom": "RPSR",
        "RandomPointsRr": "RPRr",
        "RandomPointsProbability": "RPP",
        "RandomPointsProbabilityRr": "RPPRr",
        "RandomPointsPreviousBest": "RPPB"
    }

    transform_to_name = {
        "Afiniczna": "A",
        "Perspektywiczna": "P"
    }

    table_start = "\\begin{table}[H]\n" +\
        "\\centering\n" +\
        "\\caption{10 najlepszych uzyskanych wyników liczby znalezionych par}\n" +\
        "\\begin{tabular}{c|c|c|c|c|c|c|c|c|c|c}\n"

    table_end = "\\end{tabular}\n" + \
        "\\end{table}\n"

    for i in range(len(param_names)):
        for r in range(10):
            val = str(getattr(results[r], param_names[i]))
            val = heur_to_name[val] if param_names[i] == "ramsac_heurestic" else val
            val = transform_to_name[val] if param_names[i] == "transformation" else val
            tables_rows[i] += (" & " + val)
        tables_rows[i] += "\\\\\n\\hline\n"

    result = table_start
    for r in tables_rows:
        result += r
    result += table_end
    return result


def draw_image(result):
    '''\begin{figure}[H]
    \centering
    \includegraphics[width=0.5\textwidth]{figures/mlynek.png}
    \caption{Plansza wykorzystywana w grze młynek}
    \label{fig:mlynek}
    \end{figure}
    '''
    result_filename = create_file_name(result)
    figure_tex = "\\begin{figure}[H]\n" +\
        "\\centering\n" +\
        "\\includegraphics[width=0.9\\textwidth]{figures/"+result_filename+".png}\n" +\
        f"\\caption{{Uzyskany rezultat, transformata: {result.transformation}, liczba par: {result.found_points}}}\n" +\
        "\\label{fig:"+result_filename+"}\n" +\
        "\\end{figure}"
    cv2.imwrite(f"./figures/{result_filename}.png", result.result_image)
    return figure_tex


def print_informations(result):
    result_information = f"\\descriptionResult{{{result.image1_name}}}" +\
        f"{{{result.image2_name}}}" +\
        f"{{{result.found_points}}}" +\
        f"{{{result.transformation}}}" +\
        f"{{{result.rammsac_iterations}}}" +\
        f"{{{result.ramsac_heurestic}" +\
        (f"(r={result.r}\\%, R={result.R}\\%)" if result.ramsac_heurestic ==
         "RandomPointsRr" else "")+f"}}" +\
        f"{{{result.max_ramsac_error}}}" +\
        f"{{{result.ramsac_filter_threshold}}}" +\
        f"{{{result.neighborhood_k}\\%}}" +\
        f"{{{result.neighborhood_p}}}" +\
        f"{{{result.p}}}" +\
        f"{{{result.w}}}" +\
        f"{{\\ref{{fig:{create_file_name(result)}}}}}" if result.found_points > 0 else "{----}"
    return result_information


def compare_heur_time(result):
    plot_start = "\\begin{figure}[H]\n" +\
        "\\centering\n" +\
        "\\begin{tikzpicture}\n" +\
        "\\begin{axis}[\nybar,\n" +\
        "enlargelimits = 0.15,\n" +\
        "width = 18cm, height = 12cm,\n" +\
        "title= {},\n" +\
        "xlabel = {Heurystyka},\n" +\
        "ylabel = {Czas},\n" +\
        "symbolic x coords = {\n" +\
        "RPSR, RPRr, RPP, RPPRr, RPPB},\n" +\
        "xtick = data,\n" +\
        "legend pos = north west,\n" +\
        "ymajorgrids= true,\n" +\
        "grid style = dashed,\n" +\
        "]\n"
    coords = dict()

    for r in result:
        if ((not r.ramsac_heurestic in coords) and
            r.transformation == "Afiniczna" and
            r.neighborhood_p == 0.5 and
            r.neighborhood_k == 10 and
            r.max_ramsac_error == 20 and
            r.ramsac_filter_threshold == 40 and
            r.p == 0.8 and
                r.w == 0.2):
            coords[r.ramsac_heurestic] = r.time

    plot = f"\\addplot coordinates {{(RPSR, {coords['RandomPointsSimpleRandom']}) (RPRr, {coords['RandomPointsRr']}) (RPP, {coords['RandomPointsProbability']}) (RPPRr, {coords['RandomPointsProbabilityRr']}) (RPPB, {coords['RandomPointsPreviousBest']})}};\n"
    plot_end = "\\legend{}\n" +\
        "\\end{axis}\n" +\
        "\\end{tikzpicture}\n" +\
        "\\caption{Czas wykonania (sec) algorytmu w zależności od użytej heurystyki}\n" +\
        "\\end{figure}\n"

    return plot_start + plot + plot_end


# def compare_heur_points(result):
#     plot_start = "\\begin{figure}[H]\n" +\
#         "\\centering\n" +\
#         "\\begin{tikzpicture}\n" +\
#         "\\begin{axis}[\nybar,\n" +\
#         "enlargelimits = 0.15,\n" +\
#         "width = 18cm, height = 12cm,\n" +\
#         "title= {},\n" +\
#         "xlabel = {Heurystyka},\n" +\
#         "ylabel = {Czas},\n" +\
#         "symbolic x coords = {\n" +\
#         "RPSR, RPRr, RPP, RPPRr, RPPB},\n" +\
#         "xtick = data,\n" +\
#         "legend pos = north west,\n" +\
#         "ymajorgrids= true,\n" +\
#         "grid style = dashed,\n" +\
#         "]\n"
#     coords = dict()

#     for r in result:
#         if ((not r.ramsac_heurestic in coords) and
#             r.transformation == "affine" and
#             r.neighborhood_p == 0.5 and
#             r.neighborhood_k == 0.5 and
#             r.max_ramsac_error == 20 and
#             r.ramsac_filter_threshold == 40 and
#             r.p == 0.8 and
#                 r.w == 0.2):
#             coords[r.ramsac_heurestic] = r.time

#     plot = f"\\addplot coordinates {{(RPSR, {coords['RandomPointsSimpleRandom']}) (RPRr, {coords['RandomPointsRr']}) (RPP, {coords['RandomPointsProbability']}) (RPPRr, {coords['RandomPointsProbabilityRr']}) (RPPB, {coords['RandomPointsPreviousBest']})}};\n"
#     plot_end = "\\legend{}\n" +\
#         "\\end{axis}\n" +\
#         "\\end{tikzpicture}\n" +\
#         "\\caption{Czas wykonania (sec) algorytmu w zależności od użytej heurystyki}\n" +\
#         "\\end{figure}\n"

#     return plot_start + plot + plot_end


def compare_heur_points_found(result):
    plot_start = "\\begin{figure}[H]\n" +\
        "\\centering\n" +\
        "\\begin{tikzpicture}\n" +\
        "\\begin{axis}[\nybar,\n" +\
        "enlargelimits = 0.15,\n" +\
        "width = 18cm, height = 12cm,\n" +\
        "title= {},\n" +\
        "xlabel = {Heurystyka},\n" +\
        "ylabel = {Liczba znalezionych punktow},\n" +\
        "symbolic x coords = {\n" +\
        "RPSR, RPRr, RPP, RPPRr, RPPB},\n" +\
        "xtick = data,\n" +\
        "legend pos = north west,\n" +\
        "ymajorgrids= true,\n" +\
        "grid style = dashed,\n" +\
        "]\n"
    coords = dict()

    for r in result:
        if ((not r.ramsac_heurestic in coords) and
            r.transformation == "Afiniczna" and
            r.neighborhood_p == 0.5 and
            r.neighborhood_k == 10 and
            r.max_ramsac_error == 20 and
            r.ramsac_filter_threshold == 40 and
            r.p == 0.8 and
                r.w == 0.2):
            coords[r.ramsac_heurestic] = r.found_points

    plot = f"\\addplot coordinates {{(RPSR, {coords['RandomPointsSimpleRandom']}) (RPRr, {coords['RandomPointsRr']}) (RPP, {coords['RandomPointsProbability']}) (RPPRr, {coords['RandomPointsProbabilityRr']}) (RPPB, {coords['RandomPointsPreviousBest']})}};\n"
    plot_end = "\\legend{}\n" +\
        "\\end{axis}\n" +\
        "\\end{tikzpicture}\n" +\
        "\\caption{Liczba znalezionych punktow w zależności od użytej heurystyki}\n" +\
        "\\end{figure}\n"

    return plot_start + plot + plot_end


def compare_heur_points_k(result):
    plot_start = "\\begin{figure}[H]\n" +\
        "\\centering\n" +\
        "\\begin{tikzpicture}\n" +\
        "\\begin{axis}[\nybar,\n" +\
        "enlargelimits = 0.15,\n" +\
        "width = 18cm, height = 12cm,\n" +\
        "title= {},\n" +\
        "xlabel = {Wielkość sąsiedztwa (\%)},\n" +\
        "ylabel = {Liczba znalezionych punktow},\n" +\
        "xtick = data,\n" +\
        "legend pos = north west,\n" +\
        "ymajorgrids= true,\n" +\
        "grid style = dashed,\n" +\
        "]\n"
    coords = dict()

    points = ""
    for r in result:
        if ((not r.neighborhood_k in coords) and
            r.transformation == "Afiniczna" and
            r.neighborhood_p == 0.5 and
            r.ramsac_heurestic == "RandomPointsSimpleRandom" and
            r.max_ramsac_error == 20 and
            r.ramsac_filter_threshold == 40 and
            r.p == 0.8 and
                r.w == 0.2):
            coords[r.neighborhood_k] = r.found_points
            points += f"({r.neighborhood_k}, {r.found_points}) "

    plot = f"\\addplot coordinates {{{points}}};\n"
    plot_end = "\\legend{}\n" +\
        "\\end{axis}\n" +\
        "\\end{tikzpicture}\n" +\
        "\\caption{Liczba znalezionych punktow w zależności od wielkości sąsiedztwa}\n" +\
        "\\end{figure}\n"

    return plot_start + plot + plot_end


def compare_heur_points_p(result):
    plot_start = "\\begin{figure}[H]\n" +\
        "\\centering\n" +\
        "\\begin{tikzpicture}\n" +\
        "\\begin{axis}[\nybar,\n" +\
        "enlargelimits = 0.15,\n" +\
        "width = 18cm, height = 12cm,\n" +\
        "title= {},\n" +\
        "xlabel = {Próg akceptacji},\n" +\
        "ylabel = {Liczba znalezionych punktow},\n" +\
        "xtick = data,\n" +\
        "legend pos = north west,\n" +\
        "ymajorgrids= true,\n" +\
        "grid style = dashed,\n" +\
        "]\n"
    coords = dict()

    points = ""
    for r in result:
        if ((not r.neighborhood_p in coords) and
            r.transformation == "Afiniczna" and
            r.neighborhood_k == 10 and
            r.ramsac_heurestic == "RandomPointsSimpleRandom" and
            r.max_ramsac_error == 20 and
            r.ramsac_filter_threshold == 40 and
            r.p == 0.8 and
                r.w == 0.2):
            coords[r.neighborhood_p] = r.found_points
            points += f"({r.neighborhood_p}, {r.found_points}) "

    plot = f"\\addplot coordinates {{{points}}};\n"
    plot_end = "\\legend{}\n" +\
        "\\end{axis}\n" +\
        "\\end{tikzpicture}\n" +\
        "\\caption{Liczba znalezionych punktow w zależności od progu akceptacji}\n" +\
        "\\end{figure}\n"

    return plot_start + plot + plot_end


def prepare_data_by_results_tables():
    results = load_data("./results/wyniki_raw")
    out_filename = "../sprawozdanie/wynikitables.tex"

    filename_results = dict()
    for result in results:
        if not result.image2_name in filename_results:
            filename_results[result.image2_name] = []
        filename_results[result.image2_name].append(result)

    sorted_results = dict()
    for name, elements in filename_results.items():
        elements.sort(key=lambda x: x.found_points, reverse=True)

    with open(out_filename, 'w') as out_file:
        pair = 1
        for name, elements in filename_results.items():
            out_file.write(f"\\subsection{{Para zdjęć nr. {pair}}}\n\n")
            pair += 1
            table = create_results_table(elements)
            out_file.write(table+"\n\n\n")
            times = compare_heur_time(elements)
            # fdksafjklsjdadkl
            out_file.write(times + "\n\n\n")
            out_file.write(compare_heur_points_found(elements) + "\n\n\n")
            out_file.write(compare_heur_points_k(elements) + "\n\n\n")
            out_file.write(compare_heur_points_p(elements) + "\n\n\n")


def prepare_data_by_results():
    results = load_data("./results/wyniki_raw")
    out_filename = "../sprawozdanie/wyniki.tex"

    filename_results = dict()
    for result in results:
        if not result.image2_name in filename_results:
            filename_results[result.image2_name] = []
        filename_results[result.image2_name].append(result)

    sorted_results = dict()
    for name, elements in filename_results.items():
        elements.sort(key=lambda x: x.found_points, reverse=True)

    with open(out_filename, 'w') as out_file:
        pair = 1
        for name, elements in filename_results.items():
            out_file.write(f"\\subsection{{Para zdjęć nr. {pair}}}\n\n")
            pair += 1
            for i in range(min(5, len(elements))):
                result = elements[i]
                description = print_informations(result)
                imaage = draw_image(result)
                out_file.write(description+"\n\n")
                out_file.write(imaage+"\n\\dotfill\n\n\n")

        # print(description)
        # print(imaage)


def prepare_data():
    results = load_data("./results/wyniki_raw")
    out_filename = "wyniki.tex"
    with open(out_filename, 'w') as out_file:
        for result in results:
            description = print_informations(result)
            imaage = draw_image(result)
            out_file.write(description+"\n\n")
            out_file.write(imaage+"\n\n\n")


# tester()
prepare_data_by_results_tables()
prepare_data_by_results()
