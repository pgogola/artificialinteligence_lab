from abc import abstractmethod, ABC
import random
import numpy as np


class IRandomPoints(ABC):
    def __init__(self, data, samples_num):
        super().__init__()
        self.data = np.array(data, )
        self.samples_num = samples_num

    @abstractmethod
    def random_points(self):
        pass

    @abstractmethod
    def feedback(self, param):
        pass

    @abstractmethod
    def init(self):
        pass


class RandomPointsRr(IRandomPoints):
    def __init__(self, data, samples_num, R_img1, r_img1, R_img2, r_img2):
        super(RandomPointsRr, self).__init__(data, samples_num)
        self.R_img1 = R_img1
        self.r_img1 = r_img1
        self.R_img2 = R_img2
        self.r_img2 = r_img2

    def random_points(self):
        r1_pow = self.r_img1**2
        r2_pow = self.r_img2**2
        R1_pow = self.R_img1**2
        R2_pow = self.R_img2**2

        def helper_function(point, data):
            return [x for x in data
                    if not (r1_pow < (point[0].coordinates[0]-x[0].coordinates[0])**2 + (point[0].coordinates[1]-x[0].coordinates[1])**2 and
                            R1_pow > (point[0].coordinates[0]-x[0].coordinates[0])**2 + (point[0].coordinates[1]-x[0].coordinates[1])**2 and
                            r2_pow < (point[1].coordinates[0]-x[1].coordinates[0])**2 + (point[1].coordinates[1]-x[1].coordinates[1])**2 and
                            R2_pow > (point[1].coordinates[0]-x[1].coordinates[0])**2 + (point[1].coordinates[1]-x[1].coordinates[1])**2)]

        first_point = self.data[np.random.choice(len(self.data))]
        close_to_first = helper_function(first_point, self.data)
        t = np.random.choice(len(close_to_first),
                             size=self.samples_num-1).astype(int)
        return np.array([first_point, close_to_first[t[0]], close_to_first[t[1]], close_to_first[t[2]]]) \
            if self.samples_num == 4 \
            else np.array([first_point, close_to_first[t[0]], close_to_first[t[1]]])

    def feedback(self, param):
        pass

    def init(self):
        pass


class RandomPointsProbability(IRandomPoints):
    def __init__(self, data, samples_num):
        super(RandomPointsProbability, self).__init__(data, samples_num)
        self.selected_counter = None
        self.probabilities = None
        self.previous_points = None

    def random_points(self):
        self.previous_points = np.random.choice(len(self.data), self.samples_num,
                                                p=self.probabilities)
        return np.array(self.data)[self.previous_points]

    def feedback(self, param):
        self.selected_counter[self.previous_points] += 1
        self.probabilities = self.selected_counter/self.selected_counter.sum()

    def init(self):
        self.selected_counter = np.ones((len(self.data), ))
        self.probabilities = np.full(
            (len(self.data), ), fill_value=1/len(self.data))
        self.previous_points = None


class RandomPointsSimpleRandom(IRandomPoints):
    def __init__(self, data, samples_num):
        super(RandomPointsSimpleRandom, self).__init__(data, samples_num)

    def random_points(self):
        return np.array(self.data)[np.random.choice(len(self.data), self.samples_num)]

    def feedback(self, param):
        pass

    def init(self):
        pass


class RandomPointsProbabilityRr(IRandomPoints):
    def __init__(self, data, samples_num, R_img1, r_img1, R_img2, r_img2):
        super(RandomPointsProbabilityRr, self).__init__(data, samples_num)
        self.R_img1 = R_img1
        self.r_img1 = r_img1
        self.R_img2 = R_img2
        self.r_img2 = r_img2

    def random_points(self):
        r1_pow = self.r_img1**2
        r2_pow = self.r_img2**2
        R1_pow = self.R_img1**2
        R2_pow = self.R_img2**2

        def helper_function(point, data):
            return [x for x in data
                    if not (r1_pow < (point[0].coordinates[0]-x[0].coordinates[0])**2 + (point[0].coordinates[1]-x[0].coordinates[1])**2 and
                            R1_pow > (point[0].coordinates[0]-x[0].coordinates[0])**2 + (point[0].coordinates[1]-x[0].coordinates[1])**2 and
                            r2_pow < (point[1].coordinates[0]-x[1].coordinates[0])**2 + (point[1].coordinates[1]-x[1].coordinates[1])**2 and
                            R2_pow > (point[1].coordinates[0]-x[1].coordinates[0])**2 + (point[1].coordinates[1]-x[1].coordinates[1])**2)]

        self.previous_points = np.random.choice(
            len(self.data), p=self.probabilities)
        first_point = self.data[self.previous_points]
        close_to_first = helper_function(first_point, self.data)
        t = np.random.choice(len(close_to_first),
                             size=self.samples_num-1).astype(int)
        return np.array([first_point, close_to_first[t[0]], close_to_first[t[1]], close_to_first[t[2]]]) \
            if self.samples_num == 4\
            else np.array([first_point, close_to_first[t[0]], close_to_first[t[1]]])

    def feedback(self, param):
        self.selected_counter[self.previous_points] += 2
        self.probabilities = self.selected_counter/self.selected_counter.sum()

    def init(self):
        self.selected_counter = np.ones((len(self.data), ))
        self.probabilities = np.full(
            (len(self.data), ), fill_value=1/len(self.data))
        self.previous_points = None


class RandomPointsPreviousBest(IRandomPoints):
    def __init__(self, data, samples_num, R_img1, r_img1, R_img2, r_img2):
        super(RandomPointsPreviousBest, self).__init__(data, samples_num)
        self.R_img1 = R_img1
        self.r_img1 = r_img1
        self.R_img2 = R_img2
        self.r_img2 = r_img2
        self.previous_best = None
        self.previous_points = None

    def random_points(self):
        R1_pow = self.R_img1**2
        R2_pow = self.R_img2**2

#         first_point = self.data[np.random.choice(len(self.data))]
#         close_to_first = helper_function(first_point, self.data)
#         t = np.random.choice(len(close_to_first),
#                              size=self.samples_num-1).astype(int)
#         return np.array([first_point, close_to_first[t[0]], close_to_first[t[1]], close_to_first[t[2]]]) \
#             if self.samples_num == 4 \
#             else np.array([first_point, close_to_first[t[0]], close_to_first[t[1]]])

        def helper_function(point, data):
            return [x for x in data
                    if not (R1_pow > (point[0].coordinates[0]-x[0].coordinates[0])**2 + (point[0].coordinates[1]-x[0].coordinates[1])**2 and
                            R2_pow > (point[1].coordinates[0]-x[1].coordinates[0])**2 + (point[1].coordinates[1]-x[1].coordinates[1])**2)]

        if self.previous_best is None:
            self.previous_points = np.array(
                self.data)[np.random.choice(len(self.data), self.samples_num)]
            return self.previous_points

        close_to_first = helper_function(self.previous_best, self.data)
        k = np.array(self.data)[np.random.choice(len(close_to_first),
                                                 size=self.samples_num).astype(int)]
        # close_to_first[t[0]], close_to_first[t[1]], close_to_first[t[2]]
        return k

    def feedback(self, param):
        self.previous_best = self.previous_points[0]

    def init(self):
        pass


def ramsac_iterations_calculation(p, w, n):
    '''p -- prawdopodobieństwo ze po oszacownej
    liczbie iteracji model jest dobrym
    (wystarczajaco) przybliczeniem danych
    w -- prawdopodobieństwo ze wylosowana nie jest
    szumem - stosunek liczby poprawnych par do
    wsystkich punktow w obrazie
    n -- liczba par potrzebnych do wyznaczenia parametru
    '''
    return int(np.log10(1-p)/np.log10(1-w**n))


def ramsac_method(data, iterations, maxerror, random_class, transformation='perspective'):
    def calculate_model_perspective(samples):
        x1 = samples[0][0].coordinates[0]
        y1 = samples[0][0].coordinates[1]
        x2 = samples[1][0].coordinates[0]
        y2 = samples[1][0].coordinates[1]
        x3 = samples[2][0].coordinates[0]
        y3 = samples[2][0].coordinates[1]
        x4 = samples[3][0].coordinates[0]
        y4 = samples[3][0].coordinates[1]

        u1 = samples[0][1].coordinates[0]
        v1 = samples[0][1].coordinates[1]
        u2 = samples[1][1].coordinates[0]
        v2 = samples[1][1].coordinates[1]
        u3 = samples[2][1].coordinates[0]
        v3 = samples[2][1].coordinates[1]
        u4 = samples[3][1].coordinates[0]
        v4 = samples[3][1].coordinates[1]

        A = np.array([[x1, y1, 1, 0, 0, 0, -u1*x1, -u1*y1],
                      [x2, y2, 1, 0, 0, 0, -u2*x2, -u2*y2],
                      [x3, y3, 1, 0, 0, 0, -u3*x3, -u3*y3],
                      [x4, y4, 1, 0, 0, 0, -u4*x4, -u4*y4],
                      [0, 0, 0, x1, y1, 1, -v1*x1, -v1*y1],
                      [0, 0, 0, x2, y2, 1, -v2*x2, -v2*y2],
                      [0, 0, 0, x3, y3, 1, -v3*x3, -v3*y3],
                      [0, 0, 0, x4, y4, 1, -v4*x4, -v4*y4]])
        b = np.array([u1, u2, u3, u4, v1, v2, v3, v4])
        params = np.append(np.matmul(np.linalg.inv(A), b), [1])
        return np.reshape(params, (3, 3))

    def calculate_model_affin(samples):
        x1 = samples[0][0].coordinates[0]
        y1 = samples[0][0].coordinates[1]
        x2 = samples[1][0].coordinates[0]
        y2 = samples[1][0].coordinates[1]
        x3 = samples[2][0].coordinates[0]
        y3 = samples[2][0].coordinates[1]

        u1 = samples[0][1].coordinates[0]
        v1 = samples[0][1].coordinates[1]
        u2 = samples[1][1].coordinates[0]
        v2 = samples[1][1].coordinates[1]
        u3 = samples[2][1].coordinates[0]
        v3 = samples[2][1].coordinates[1]

        A = np.array([[x1, y1, 1, 0, 0, 0],
                      [x2, y2, 1, 0, 0, 0],
                      [x3, y3, 1, 0, 0, 0],
                      [0, 0, 0, x1, y1, 1],
                      [0, 0, 0, x2, y2, 1],
                      [0, 0, 0, x3, y3, 1]])
        b = np.array([u1, u2, u3, v1, v2, v3])
        params = np.append(np.matmul(np.linalg.inv(A), b), [1])
        return np.array([params[0:3],
                         params[3:6],
                         [0, 0, params[6]]])

    def modelerror(model, data):
        v_prim = np.matmul(model, np.array(
            [data[0].coordinates[0], data[0].coordinates[1], 1]))
        v_prim /= v_prim[2]
        # odleglosc euklidesowa
        return np.linalg.norm(np.array([data[1].coordinates[0], data[1].coordinates[1]])-v_prim[:2])

    if 'affine' == transformation:
        samples_num = 3
        model_calc = calculate_model_affin
    elif 'perspective' == transformation:
        samples_num = 4
        model_calc = calculate_model_perspective
    else:
        raise ""
    bestmodel = None
    bestscore = 0
    iter_m = 0
    for i in range(iterations):
        model = None
        while model is None:
            s = random_class.random_points()
            try:
                model = model_calc(s)
            except:
                model = None
        score = 0
        for d in data:
            error = modelerror(model, d)
            if error < maxerror:
                score += 1
        iter_m += 1
        if score > bestscore:
            random_class.feedback(None)
            bestscore = score
            bestmodel = model
            iter_m = 0
        if iter_m > 5000:
            return bestmodel
    return bestmodel
