#include <utility>

//
// Created by piotr on 29.05.19.
//

#ifndef PROJECTCPP_IMAGE_H
#define PROJECTCPP_IMAGE_H

#include <string>
#include <valarray>
#include <vector>

class CharacteristicPoint { ;
public:
    CharacteristicPoint(std::pair<float, float> coordinates, std::valarray<float> featureVector)
            : coordinates{std::move(coordinates)}, featureVector{std::move(featureVector)}{}

    const std::pair<float, float> &getCoordinates() const {
        return coordinates;
    }

    void setCoordinates(const std::pair<float, float> &coordinates) {
        CharacteristicPoint::coordinates = coordinates;
    }

    const std::valarray<float> &getFeatureVector() const {
        return featureVector;
    }

    void setFeatureVector(const std::valarray<float> &featureVector) {
        CharacteristicPoint::featureVector = featureVector;
    }

private:
    std::pair<float, float> coordinates;
    std::valarray<float> featureVector;
};


class Image {
public:
    explicit Image(std::string imagename) : imagename{std::move(imagename)} {}

    void extractCharacteristicPoints(bool draw = false);

    void loadCharacteristicPoints();

    const std::string &getImagename() const {
        return imagename;
    }

    void setImagename(const std::string &imagename) {
        Image::imagename = imagename;
    }

    const std::vector<CharacteristicPoint> &getCharacteristicPoints() const {
        return characteristicPoints;
    }

    void setCharacteristicPoints(const std::vector<CharacteristicPoint> &characteristicPoints) {
        Image::characteristicPoints = characteristicPoints;
    };

private:
    std::string imagename;
    std::vector<CharacteristicPoint> characteristicPoints;

};


#endif //PROJECTCPP_IMAGE_H
