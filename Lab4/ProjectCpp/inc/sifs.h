//
// Created by piotr on 30.05.19.
//

#ifndef PROJECTCPP_SIFS_H
#define PROJECTCPP_SIFS_H

#include <vector>
#include <fstream>
#include "Image.h"

std::vector<std::pair<CharacteristicPoint, CharacteristicPoint>> distanceMatrix(const Image &image1, const Image &image2);

//template<typename T> void saveMatrix(const std::vector<std::vector<T>>& matrix, std::string fileName);
//
//template<typename T> std::vector<std::vector<T>> readMatrix(std::string fileName);


template<typename T>
void saveMatrix(const std::vector<std::vector<T>>& matrix, std::string fileName){
    std::ofstream out(fileName);//(char*)&obj, sizeof(obj)
    const char * x = reinterpret_cast<const char*>(&matrix);
    out.write(x, sizeof(matrix));
    out.close();


    out.open(fileName + "_txt");
    for(auto w : matrix){
        for(auto k : w){
            out << k << " ";
        }
        out << "\n";
    }
    out.close();
}

template<typename T>
std::vector<std::vector<T>> readMatrix(std::string fileName){
    std::ifstream in(fileName);//(char*)&obj, sizeof(obj)
    in.seekg (0, in.end);
    int length = in.tellg();
    in.seekg (0, in.beg);
    char * mem = new char[length];
    in.read(mem, length);
    auto * vec = reinterpret_cast<std::vector<std::vector<T>>*>(mem);
    return *vec;
}
#endif //PROJECTCPP_SIFS_H
