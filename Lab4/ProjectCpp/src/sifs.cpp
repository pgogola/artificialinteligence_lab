//
// Created by piotr on 30.05.19.
//

#include <iostream>
#include <thread>
#include <mutex>
#include <fstream>
#include "../inc/sifs.h"

using std::vector;
using std::thread;
using std::ifstream;
using std::ofstream;
//def distance_matrix(image1, image2):
//img1_points_len = len(image1.characteristic_points)
//img2_points_len = len(image2.characteristic_points)
//distances = np.zeros((img1_points_len, img2_points_len), dtype=np.float16)
//for img1_idx in range(img1_points_len):
//for img2_idx in range(img2_points_len):
//distances[img1_idx, img2_idx] = np.sum(
//        np.subtract(image1.characteristic_points[img1_idx].feature_vector, image2.characteristic_points[img2_idx].feature_vector)**2)


std::vector<std::pair<CharacteristicPoint, CharacteristicPoint>>
distanceMatrix(const Image &image1, const Image &image2) {
    int img1PointsLen = image1.getCharacteristicPoints().size();
    int img2PointsLen = image2.getCharacteristicPoints().size();
    vector<std::pair<CharacteristicPoint, CharacteristicPoint>> points;

    for (int img1BaseIdx = 0; img1BaseIdx < img1PointsLen; img1BaseIdx++) {
        float minDistanceImg1to2 = std::numeric_limits<float>::infinity();
        int minDistanceImg1to2idx = -1;

        for (int img2Idx = 0; img2Idx < img2PointsLen; img2Idx++) {
            auto distance = std::pow(image1.getCharacteristicPoints()[img1BaseIdx].getFeatureVector() -
                                     image2.getCharacteristicPoints()[img2Idx].getFeatureVector(), 2).sum();
            if (distance < minDistanceImg1to2) {
                minDistanceImg1to2 = distance;
                minDistanceImg1to2idx = img2Idx;
            }
        }

        float minDistanceImg2to1 = std::numeric_limits<float>::infinity();
        int minDistanceImg2to1idx = -1;
        for (int img1Idx = 0; img1Idx < img1PointsLen; img1Idx++) {
            auto distance = std::pow(image2.getCharacteristicPoints()[minDistanceImg1to2idx].getFeatureVector() -
                                     image1.getCharacteristicPoints()[img1Idx].getFeatureVector(), 2).sum();
            if (distance < minDistanceImg2to1) {
                minDistanceImg2to1 = distance;
                minDistanceImg2to1idx = img1Idx;
            }
            if (minDistanceImg2to1 < minDistanceImg1to2) {
                break;
            }

        }

        if (minDistanceImg2to1idx == img1BaseIdx) {
            points.emplace_back(image1.getCharacteristicPoints()[minDistanceImg2to1idx],
                                image2.getCharacteristicPoints()[minDistanceImg1to2idx]);
        }

        std::cout << img1BaseIdx << "\n";


    }


//    auto calculateDistances = [&](int from, int to){
//         for (int img1Idx = from; img1Idx < to; img1Idx++) {
//            distances.at(img1Idx) = std::vector<float>(img2PointsLen);
//            for (int img2Idx = 0; img2Idx < img2PointsLen; img2Idx++) {
//
//                auto distance = std::pow(image1.getCharacteristicPoints()[img1Idx].getFeatureVector() -
//                                         image2.getCharacteristicPoints()[img2Idx].getFeatureVector(), 2).sum();
//
//                distances.at(img1Idx).emplace_back(distance);
//
//            }
//        }
//    };
//    thread upperPart = thread(calculateDistances, 0, img1PointsLen/3);
//    thread middlePart = thread(calculateDistances, img1PointsLen/3, img1PointsLen*2/3);
//    thread lowerPart = thread(calculateDistances, img1PointsLen*2/3, img1PointsLen);
//    upperPart.join();
//    middlePart.join();
//    lowerPart.join();
    return points;
}

//template<typename T>
//void saveMatrix(const std::vector<std::vector<T>>& matrix, std::string fileName){
//    ofstream out(fileName);//(char*)&obj, sizeof(obj)
//    const char * x = reinterpret_cast<const char*>(&matrix);
//    out.write(x, sizeof(matrix));
//    out.close();
//}
//
//template<typename T>
//std::vector<std::vector<T>> readMatrix(std::string fileName){
//    ifstream in(fileName);//(char*)&obj, sizeof(obj)
//    in.seekg (0, in.end);
//    int length = in.tellg();
//    in.seekg (0, in.beg);
//    char * mem = new char[length];
//    in.read(mem, length);
//    auto * vec = reinterpret_cast<std::vector<std::vector<T>>*>(mem);
//    return *vec;
//}