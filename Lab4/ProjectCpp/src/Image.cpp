//
// Created by piotr on 29.05.19.
//

#include <cstdlib>
#include <fstream>
#include <sstream>
#include <iostream>
#include "../inc/Image.h"


std::string rootFolder = "/home/piotr/Desktop/Studia/Sztuczna_Inteligencja_i_Inżynieria_Wiedzy/Laboratorium/Lab4/ProjectCpp";

void Image::extractCharacteristicPoints(bool draw) {
    auto featureExtractor = rootFolder + "/extract_features/extract_features_32bit.ln -haraff -sift -i " +
                            this->imagename + (draw ? " -DE" : "");
    std::system(featureExtractor.c_str());
};

void Image::loadCharacteristicPoints() {
    auto featureVectorsFileName = this->imagename + ".haraff.sift";
    std::ifstream featuresFile(featureVectorsFileName);
    int simpleVectorSize;
    featuresFile >> simpleVectorSize;
    int vectorsQty;
    featuresFile >> vectorsQty;
    std::string line;
    while(std::getline(featuresFile, line)){
        if(line.empty()){
            continue;
        }
        std::stringstream newVector(line);
        float xCoord, yCoord, tmp;
        newVector >> xCoord;
        newVector >> yCoord;
        newVector >> tmp;
        newVector >> tmp;
        newVector >> tmp;
        std::valarray<float> featuresVector(simpleVectorSize);
        int i = 0;
        for(;  newVector >> tmp && i< simpleVectorSize; i++){
            featuresVector[i] = tmp;
        }
        if(i < simpleVectorSize){
//            std::cout << i;
            throw "simple vector size";
        }
        CharacteristicPoint characteristicPoint({xCoord, yCoord}, featuresVector);
        this->characteristicPoints.emplace_back(characteristicPoint);
    }
    featuresFile.close();
}