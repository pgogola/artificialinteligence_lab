#include <iostream>
#include "inc/Image.h"
#include "inc/sifs.h"
#include <chrono>

std::string rootFolder1 = "/home/piotr/Desktop/Studia/Sztuczna_Inteligencja_i_Inżynieria_Wiedzy/Laboratorium/Lab4/ProjectCpp";

int main() {
    auto imageName1 = rootFolder1 + "/images/kaczuszka31.png";
    auto imageName2 = rootFolder1 + "/images/kaczuszka41.png";
    Image image1(imageName1);
    Image image2(imageName2);
    image1.extractCharacteristicPoints();
    image2.extractCharacteristicPoints();
    image1.loadCharacteristicPoints();
    image2.loadCharacteristicPoints();
    auto start = std::chrono::steady_clock::now();
    auto distances = distanceMatrix(image1, image2);
    auto end = std::chrono::steady_clock::now();
    std::cout << std::chrono::duration_cast<std::chrono::seconds>(end-start).count();
//    saveMatrix(distances, "distances");
    return 0;
}