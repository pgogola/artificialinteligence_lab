import pickle
from tester import TestCase, PlayerCase
import os
import random


def load_data(file_name):
    with open(file_name, "rb") as f:
        d = pickle.load(f)
    return d

# \definecolor{britishracinggreen}{rgb}{0.0, 0.26, 0.15}


def generate_table_row(data, lp):
    names = {
        "alpha_beta": "$\\alpha{}\\beta{}$",
        "min_max": "MinMax",
        "state_rating_pieces": "\\cellcolor{blue!25}SRLP",
        "state_rating_blocked": "\\cellcolor{green!25}SRBP",
        "alpha_beta_sort_mill_number": "\\cellcolor{magenta!25}$\\alpha{}\\beta{}SMN$",
        "alpha_beta_sort_pieces_number": "\\cellcolor{lime!25}$\\alpha{}\\beta{}SPN$",
        None: "----"
    }
    p1_times = 0
    p2_times = 0
    p1_wins = 0
    p2_wins = 0
    p1_moves = 0
    p2_moves = 0
    p1_moves = 0
    p2_moves = 0

    for i in range(len(data)-1):
        if data[i].player1.ratting_fun != data[i+1].player1.ratting_fun or\
                data[i].player1.alg_fun != data[i+1].player1.alg_fun or\
                data[i].player1.alpha_beta_heur != data[i+1].player1.alpha_beta_heur or\
                data[i].player2.ratting_fun != data[i+1].player2.ratting_fun or\
                data[i].player2.alg_fun != data[i+1].player2.alg_fun or\
                data[i].player2.alpha_beta_heur != data[i+1].player2.alpha_beta_heur:
            raise Exception("Niezgodnosc wywolan")

    for g in data:
        p1_times += g.player1.time
        p2_times += g.player2.time
        p1_wins += (1 if g.player1.isWinner else 0)
        p2_wins += (1 if g.player2.isWinner else 0)
        p1_moves += g.player1.moves
        p2_moves += g.player2.moves

    p1_mean_movemenents = p1_moves/len(data)
    p2_mean_movemenents = p2_moves/len(data)
    p1_mean_time = p1_times/len(data)
    p2_mean_time = p2_times/len(data)

    p1 = data[0].player1
    p2 = data[0].player2
    lp = "\\multirow{2}{*}{\\textbf{" + str(lp) + "}}"
    pl1 = " & P1 & " + names[p1.alg_fun] + " & " +\
        str(names[p1.ratting_fun]) + " & " +\
        str(p1.deepth) + " & " +\
        "{0:.4f}".format(p1_mean_time/p1_mean_movemenents) + " & " +\
        str(p1_mean_movemenents) + \
        " & " + names[p1.alpha_beta_heur] + " & " + \
        str(p1_wins) + "/" + str(len(data)) + " \\\\\n\\cline{2-9}\n"
    pl2 = " & P2 & " + names[p2.alg_fun] + " & " +\
        names[p2.ratting_fun] + " & " +\
        str(p2.deepth) + " & " +\
        "{0:.4f}".format(p2_mean_time/p2_mean_movemenents) + " & " +\
        str(p2_mean_movemenents) + \
        " & " + names[p2.alpha_beta_heur] + " & " + \
        str(p2_wins) + "/" + str(len(data)) + " \\\\\n\\hline\n\\hline"

    return lp + pl1 + pl2


def generate_plot_data_series(data, play_select=None):
    names = {
        "alpha_beta": "$\\alpha{}\\beta{}$",
        "min_max": "MinMax",
        "state_rating_pieces": "SRLP",
        "state_rating_blocked": "SRBP",
        "alpha_beta_sort_mill_number": "$\\alpha{}\\beta{}$SMN",
        "alpha_beta_sort_pieces_number": "$\\alpha{}\\beta{}$SPN",
        None: "----"
    }
    p1_times = 0
    p2_times = 0
    p1_wins = 0
    p2_wins = 0
    p1_moves = 0
    p2_moves = 0
    p1_moves = 0
    p2_moves = 0

    for i in range(len(data)-1):
        if data[i].player1.ratting_fun != data[i+1].player1.ratting_fun or\
                data[i].player1.alg_fun != data[i+1].player1.alg_fun or\
                data[i].player1.alpha_beta_heur != data[i+1].player1.alpha_beta_heur or\
                data[i].player2.ratting_fun != data[i+1].player2.ratting_fun or\
                data[i].player2.alg_fun != data[i+1].player2.alg_fun or\
                data[i].player2.alpha_beta_heur != data[i+1].player2.alpha_beta_heur:
            raise Exception("Niezgodnosc wywolan")

    for g in data:
        p1_times += g.player1.time
        p2_times += g.player2.time
        p1_wins += (1 if g.player1.isWinner else 0)
        p2_wins += (1 if g.player2.isWinner else 0)
        p1_moves += g.player1.moves
        p2_moves += g.player2.moves

    p1_mean_movemenents = p1_moves/len(data)
    p2_mean_movemenents = p2_moves/len(data)
    p1_mean_time = p1_times/len(data)
    p2_mean_time = p2_times/len(data)

    p1 = data[0].player1
    p2 = data[0].player2

    player = (p1 if play_select == "p1" else p2)
    legend = names[p1.alg_fun] + "-" + names[p1.ratting_fun] + "-" + \
        ("" if player.alpha_beta_heur is None else names[player.alpha_beta_heur] +
         "-") + str(player.deepth) + "-" + ("1" if player.isWinner else "0")
    color = "blue"
    mark = "square"
    coordinates = ""
    for i in range(len(player.scores)):
        coordinates += "(" + str(i) + ", " + str(player.scores[i]) + ")"

    color = random.choice(["black", "blue", "brown", "cyan", "darkgray", "gray", "green", "lightgray", "lime",
                           "magenta", "olive", "orange", "pink", "purple", "red", "teal", "violet", "yellow"])
    mark = random.choice(["triangle", "squre"])
    result = "\\addplot[\n" +\
        "\tcolor=" + color + ",\n" +\
        "\tmark="+mark + ",\n" +\
        "\t]\n" +\
        "\tcoordinates {\n" +\
        "\t" + coordinates + "\n" +\
        "\t};\n" +\
        "\t\\addlegendentry{"+legend+"}"
    return result


directory = "./results/"

files = [
    "min_max_only_same_depth_dif_ratting2.dat",
    "min_max_only_same_depth_dif_ratting3.dat",
    "min_max_only_same_depth_dif_ratting4.dat",
    "alphabeta_minmax_same_depth_same_ratting2.dat",
    "alphabeta_minmax_same_depth_same_ratting3.dat",
    "alphabeta_minmax_same_depth_same_ratting4.dat",
    "alphabeta_only_same_depth_diff_ratting2_dif_heur2.dat",
    "alphabeta_only_same_depth_diff_ratting2_dif_heur3.dat",
    "alphabeta_only_same_depth_diff_ratting2_dif_heur4.dat",
    "alphabeta_only_same_depth_diff_ratting_dif_heur2.dat",
    "alphabeta_only_same_depth_diff_ratting_dif_heur3.dat",
    "alphabeta_only_same_depth_diff_ratting_dif_heur4.dat",
    "alphabeta_only_same_depth_same_ratting_dif_heur2.dat",
    "alphabeta_only_same_depth_same_ratting_dif_heur3.dat",
    "alphabeta_only_same_depth_same_ratting_dif_heur4.dat",
    "alphabeta_thesame_diff_deepth2.dat",
    "alphabeta_thesame_diff_deepth3.dat",
    "alphabeta_thesame_diff_deepth4.dat",
]

lp = 1
files_result = os.listdir(directory)
files_result.sort()
for file_name in files:
    data = load_data(directory + file_name)
    for i in range(0, len(data), 5):

        # print(str(data))
        # print(generate_table_row(data[i:i+5], lp))
        # print(generate_plot_data_series(data, "p1"))
        # print(generate_plot_data_series(data, "p2"))
        lp += 1


all_data = []
algorithms_comb = set()
for file_name in files:
    data = load_data(directory + file_name)
    for d in data:
        p1 = d.player1
        p2 = d.player2
        all_data.append(p1)
        all_data.append(p2)
        algorithms_comb.add((p1.alg_fun, p1.ratting_fun,
                             p1.alpha_beta_heur, p1.deepth))
        algorithms_comb.add((p2.alg_fun, p2.ratting_fun,
                             p2.alpha_beta_heur, p2.deepth))

names = {
    "alpha_beta": "$\\alpha{}\\beta{}$",
    "min_max": "MinMax",
    "state_rating_pieces": "SRLP",
    "state_rating_blocked": "SRBP",
    "alpha_beta_sort_mill_number": "$\\alpha{}\\beta{}$SMN",
    "alpha_beta_sort_pieces_number": "$\\alpha{}\\beta{}$SPN",
    None: "----"
}

for ac in algorithms_comb:
    it = 0
    t_sum = 0
    for p in all_data:
        if (p.alg_fun, p.ratting_fun, p.alpha_beta_heur, p.deepth) == ac:
            it += 1
            t_sum += p.time/p.moves
    print(str(ac) + " " + str(t_sum/it))
