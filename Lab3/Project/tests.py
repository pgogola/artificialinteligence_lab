import copy
import time
import min_max as mm
import Board as bo

# C     W     W
#   C   C   +
#     C + +
# + + C   + + +
#     C + +
#   +   C   +
# C     W     C

board = bo.Board()

board.add_pawn(bo.Pawn(bo.PawnColor.BLACK), 0, 0)
board.add_pawn(bo.Pawn(bo.PawnColor.BLACK), 1, 1)
board.add_pawn(bo.Pawn(bo.PawnColor.BLACK), 2, 2)
board.add_pawn(bo.Pawn(bo.PawnColor.BLACK), 3, 1)
board.add_pawn(bo.Pawn(bo.PawnColor.BLACK), 4, 2)
board.add_pawn(bo.Pawn(bo.PawnColor.BLACK), 6, 0)

board.add_pawn(bo.Pawn(bo.PawnColor.WHITE), 3, 0)
board.add_pawn(bo.Pawn(bo.PawnColor.WHITE), 6, 6)
board.add_pawn(bo.Pawn(bo.PawnColor.WHITE), 6, 3)
board.add_pawn(bo.Pawn(bo.PawnColor.WHITE), 3, 6)

print(board)
# s, b = mm.alpha_beta(board, mm.generate_possible_states,
#                      mm.basic_state_rating, bo.PawnColor.BLACK, 3)
# print(s)
# print(b)


print(mm.number_of_two_pieces_mill(board, bo.PawnColor.BLACK))
