from Board import PawnColor, Pawn
import operator

coordinates_for_pieces = [
    [(0, 0), (0, 3), (0, 6)],
    [(1, 1), (1, 3), (1, 5)],
    [(2, 2), (2, 3), (2, 4)],
    [(3, 0), (3, 1), (3, 2)],
    [(3, 4), (3, 5), (3, 6)],
    [(4, 2), (4, 3), (4, 4)],
    [(5, 1), (5, 3), (5, 5)],
    [(6, 0), (6, 3), (6, 6)],

    [(0, 0), (3, 0), (6, 0)],
    [(1, 1), (3, 1), (5, 1)],
    [(2, 2), (3, 2), (4, 2)],
    [(0, 3), (1, 3), (2, 3)],
    [(4, 3), (5, 3), (6, 3)],
    [(2, 4), (3, 4), (4, 4)],
    [(1, 5), (3, 5), (5, 5)],
    [(0, 6), (3, 6), (6, 6)],

]


maxdepth = 100


def generate_possible_states(board, player_color):

    player_pawns = board.pawns[player_color]
    oponent_color = PawnColor.BLACK if player_color == PawnColor.WHITE else PawnColor.WHITE
    pplen = len(player_pawns)
    if len(board.pawns[PawnColor.BLACK]) < 3 or len(board.pawns[PawnColor.WHITE]) < 3:
        return []
    possible_states = []
    for pawn in player_pawns:
        for movement in (board.coordinates if pplen == 3 else board.possible_moves[pawn.coordinates[0]*10+pawn.coordinates[1]]):

            if board.prev[player_color] is not None and board.prev[player_color] == (pawn, movement):
                continue
            if board.get_pawn(movement[0], movement[1]) is None:
                board_copy = board.copy()
                board_copy.remove_pawn(pawn)
                board_copy.prev[player_color] = (
                    Pawn(pawn.color, movement), pawn.coordinates)
                board_copy.add_pawn(Pawn(pawn.color), movement[0], movement[1])

                if board_copy.isMill(movement):
                    for op_pawn in board_copy.pawns[oponent_color]:
                        board_op_copy = board_copy.copy()
                        board_op_copy.remove_pawn(op_pawn)
                        possible_states.append(board_op_copy)
                else:
                    possible_states.append(board_copy)
    return possible_states


def generate_possible_states_first_phase(board, player_color):
    possible_states = []
    oponent_color = PawnColor.BLACK if player_color == PawnColor.WHITE else PawnColor.WHITE
    for movement in board.coordinates:
        if board.get_pawn(movement[0], movement[1]) is None:
            board_copy = board.copy()
            board_copy.add_pawn(Pawn(player_color), movement[0], movement[1])

            millsNum = board_copy.isMill(movement)
            if millsNum > 0:
                for op_pawn in board_copy.pawns[oponent_color]:
                    board_op_copy = board_copy.copy()
                    board_op_copy.remove_pawn(op_pawn)
                    if millsNum == 2:
                        for op_pawn_s in board_op_copy.pawns[oponent_color]:
                            board_op_copy_s = board_op_copy.copy()
                            board_op_copy_s.remove_pawn(op_pawn_s)
                            possible_states.append(board_op_copy_s)
                    else:
                        possible_states.append(board_op_copy)
            else:
                possible_states.append(board_copy)

    return possible_states


def number_of_pieces(board, player_color):
    return len(board.pawns[player_color])


def number_of_blocked_pieces(board, player_color):
    blocked = 0
    for i in board.pawns[player_color]:
        blocked_all = True
        for c in board.possible_moves[i.coordinates[0]*10+i.coordinates[1]]:
            if board.get_pawn(c[0], c[1]) is None:
                blocked_all = False
        if blocked_all == True:
            blocked += 1
    return blocked


def number_of_two_pieces_mill(board, player_color):
    result = 0
    for rowcol in coordinates_for_pieces:
        for_color = 0
        for elem in rowcol:
            if board.get_pawn(elem[0], elem[1]) is not None and board.get_pawn(elem[0], elem[1]).color == player_color:
                for_color += 1
            elif board.get_pawn(elem[0], elem[1]) is not None:
                break
        if for_color == 2:
            result += 1
    return result


def playerState(board, player_color):
    if len(board.pawns[player_color]) < 3:
        return -1
    elif len(board.pawns[PawnColor.BLACK if PawnColor.WHITE == player_color else PawnColor.WHITE]) < 3:
        return 1
    return 0


def state_rating_pieces(board, current_player, player_moves, current_deepth):
    opplayer_color = PawnColor.BLACK if PawnColor.WHITE == current_player else PawnColor.WHITE
    left_pieces = 30 * (number_of_pieces(board, current_player) -
                        number_of_pieces(board, opplayer_color))
    mills = 10 * (number_of_two_pieces_mill(board, current_player) -
                  number_of_two_pieces_mill(board, opplayer_color))
    blocked = 5 * (number_of_blocked_pieces(board, opplayer_color) -
                   number_of_blocked_pieces(board, current_player))
    state = 1000 * \
        playerState(board, current_player) if player_moves + \
        current_deepth >= 8 else 0
    return left_pieces + mills + blocked + state


def state_rating_blocked(board, current_player, player_moves, current_deepth):
    opplayer_color = PawnColor.BLACK if PawnColor.WHITE == current_player else PawnColor.WHITE
    left_pieces = 15 * (number_of_pieces(board, current_player) -
                        number_of_pieces(board, opplayer_color))
    mills = 5 * (number_of_two_pieces_mill(board, current_player) -
                 number_of_two_pieces_mill(board, opplayer_color))
    blocked = 25 * (number_of_blocked_pieces(board, opplayer_color) -
                    number_of_blocked_pieces(board, current_player))
    state = 1000 * \
        playerState(board, current_player) if player_moves + \
        current_deepth >= 8 else 0
    return left_pieces + mills + blocked + state


def min_max(init_state, possible_modification_gen, state_rating_fun, current_player, max_depth, player_move, *argv):
    nextStep = None

    def min_max_helper(current_state, player, depth):
        if depth >= max_depth:
            return state_rating_fun(current_state, current_player, depth+player_move, depth)
        scores = float('inf')
        if current_player == player:
            scores *= -1
        comp_fun = max if (current_player == player) else min

        possible_states = possible_modification_gen(current_state, player)
        if not possible_states:
            return state_rating_fun(current_state, current_player, depth+player_move, depth)
        for state in possible_states:
            scores = comp_fun(scores, min_max_helper(
                state, (PawnColor.BLACK if player == PawnColor.WHITE else PawnColor.WHITE), depth+1))
        return scores

    scores = -float('inf')
    possible_states = possible_modification_gen(init_state, current_player)
    if not possible_states:
        return state_rating_fun(init_state, current_player, player_move, 0), nextStep

    for state in possible_states:
        possibilityScores = min_max_helper(
            state, (PawnColor.BLACK if current_player == PawnColor.WHITE else PawnColor.WHITE), 1)
        if possibilityScores > scores:
            scores = possibilityScores
            nextStep = state
    return scores, nextStep


def alpha_beta(init_state, possible_modification_gen, state_rating_fun, current_player, max_depth, player_move, sort_alg):
    nextStep = None

    def alpha_beta_helper(current_state, player, depth, alpha, beta):
        if depth >= max_depth:
            return state_rating_fun(current_state, current_player, depth+player_move, depth)
        scores = float('inf')
        if current_player == player:
            scores *= -1
        comp_fun = max if (current_player == player) else min

        possible_states = possible_modification_gen(
            current_state, player)
        if not possible_states:
            return state_rating_fun(current_state, current_player, depth+player_move, depth)
        for state in sort_alg(possible_states, current_player):
            scores = comp_fun(scores, alpha_beta_helper(
                state, (PawnColor.BLACK if player == PawnColor.WHITE else PawnColor.WHITE), depth+1, alpha, beta))
            if current_player == player:
                alpha = max(alpha, scores)
            else:
                beta = min(beta, scores)
            if alpha >= beta:
                break
        return scores

    scores = -float('inf')
    possible_states = possible_modification_gen(init_state, current_player)
    if not possible_states:
        return state_rating_fun(init_state, current_player, player_move, 0), nextStep

    for state in sort_alg(possible_states, current_player):
        possibilityScores = alpha_beta_helper(
            state, (PawnColor.BLACK if current_player == PawnColor.WHITE else PawnColor.WHITE), 1, -float('inf'), float('inf'))
        if possibilityScores > scores:
            scores = possibilityScores
            nextStep = state
    return scores, nextStep


def alpha_beta_sort_pieces_number(states, current_player):
    value_state = []
    for state in states:
        value_state.append((state, number_of_pieces(state, current_player) - number_of_pieces(
            state, PawnColor.BLACK if current_player == PawnColor.WHITE else PawnColor.WHITE)))
    value_state.sort(key=operator.itemgetter(1), reverse=True)
    return [v[0] for v in value_state]


def alpha_beta_sort_mill_number(states, current_player):
    value_state = []
    for state in states:
        value_state.append((state, number_of_two_pieces_mill(state, current_player) - number_of_two_pieces_mill(
            state, PawnColor.BLACK if current_player == PawnColor.WHITE else PawnColor.WHITE)))
    value_state.sort(key=operator.itemgetter(1), reverse=True)
    return [v[0] for v in value_state]
