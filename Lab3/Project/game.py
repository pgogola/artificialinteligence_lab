import copy
import time
import min_max as mm
import Board as bo
import enum


class GAME_TYPE(enum.Enum):
    PvsAI = 0,
    AIvsAI = 1


class GAME_PHASE(enum.Enum):
    FIRST = 0,
    SECOND = 1,
    THIRD = 2


class Player:
    def __init__(self, color):
        self.movements = 0
        self.color = color
        self.lost = False

    def move(self):
        pass


class PlayerAI(Player):
    def __init__(self, color, state_ratting, algorithm, max_depth, alpha_beta_alg_heur=None):
        super().__init__(color)
        self.state_ratting = state_ratting
        self.algorithm = algorithm
        self.max_depth = max_depth
        self.alpha_beta_heur = alpha_beta_alg_heur

    def move(self, board):
        states_generator = mm.generate_possible_states if self.movements > 8 else mm.generate_possible_states_first_phase
        self.movements += 1
        scores, board = self.algorithm(
            board, states_generator, self.state_ratting, self.color, self.max_depth, self.movements, self.alpha_beta_heur)
        self.lost = board is None
        return board, scores


# player1 = PlayerAI(bo.PawnColor.BLACK,
#                    mm.basic_state_rating, mm.alpha_beta, 4, mm.alpha_beta_sort_mill_number)
# player2 = PlayerAI(bo.PawnColor.WHITE,
#                    mm.basic_state_rating, mm.alpha_beta, 4, mm.alpha_beta_sort_pieces_number)
# board = bo.Board()

# print(player1.algorithm.__name__)

# while not (player1.lost or player2.lost):
#     print("\nblack")
#     board = player1.move(board)
#     print(board)
#     if player1.lost:
#         print("black lost")
#         break
#     print("\nwhite")
#     board = player2.move(board)
#     print(board)
#     if player2.lost:
#         print("white lost")
#         break

#     if(player1.movements*player2.movements > 64 and len(board.pawns[bo.PawnColor.BLACK]) <= 3 and len(board.pawns[bo.PawnColor.WHITE]) <= 3):
#         break

# print(str(player1.movements))
# print(str(player2.movements))

# class Game():
#     def __init__(self, player1, player2):
#         self.player1 = player1
#         self.player2 = player2
#         # self.algorithm_AI_2 = algorithm_AI_2
#         # self.game_type = game_type
#         # self.max_depth_AI_1 = max_depth_AI_1
#         # self.max_depth_AI_2 = max_depth_AI_2
#         # self.player_1_moves = None
#         # self.player_2_moves = None
#         # self.board = None
#         # self.game_phase = None
#         # self.player1_color = player1_color
#         # self.player2_color = player2_color
#         # self.current_player = None

#     def initialize_game(self):
#         self.board = bo.Board()
#         self.player_1_moves = 0
#         self.player_2_moves = 0
#         self.current_player = self.player1_color

#     def move_AI(self, player, max_depth, states_generator):
#         return self.algorithm(self.board, states_generator, self.state_ratting, player, max_depth)

#     def next_move(self):
#         if self.current_player == self.player1_color:
#             if self.player_1_moves < 8
#             self.player_1_moves += 1


# game = Game()
