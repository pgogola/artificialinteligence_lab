import numpy as np
import enum
import copy


def empty_copy(obj):
    class Empty(obj.__class__):
        def __init__(self):
            self.board = obj.board.copy()
            self.pawns = copy.deepcopy(obj.pawns)
            self.prev = obj.prev.copy()
    newcopy = Empty()
    newcopy.__class__ = obj.__class__

    return newcopy


class PawnColor(enum.Enum):
    BLACK = 0
    WHITE = 1
    NONE = -1


class Pawn:
    def __init__(self, color, coordinates=None):
        self.color = color
        self.coordinates = coordinates

    def __eq__(self, other):
        return self.coordinates == other.coordinates and self.color == other.color

    def __hash__(self):
        return hash((self.coordinates, self.color))

    def __ne__(self, other):
        return self.coordinates != other.coordinates or self.color != other.color


class Board:
    coordinates = [
        (0, 0), (0, 3), (0, 6),
        (1, 1), (1, 3), (1, 5),
        (2, 2), (2, 3), (2, 4),
        (3, 0), (3, 1), (3, 2), (3, 4), (3, 5), (3, 6),
        (4, 2), (4, 3), (4, 4),
        (5, 1), (5, 3), (5, 5),
        (6, 0), (6, 3), (6, 6)
    ]

    possible_moves = {
        0: [(0, 3), (3, 0)],
        3: [(0, 0), (0, 6), (1, 3)],
        6: [(0, 3), (3, 6)],
        11: [(3, 1), (1, 3)],
        13: [(0, 3), (2, 3), (1, 1), (1, 5)],
        15: [(1, 3), (3, 5)],
        22: [(3, 2), (2, 3)],
        23: [(2, 2), (2, 4), (1, 3)],
        24: [(2, 3), (3, 4)],
        30: [(0, 0), (6, 0), (3, 1)],
        31: [(3, 0), (3, 2), (1, 1), (5, 1)],
        32: [(2, 2), (4, 2), (3, 1)],
        34: [(2, 4), (4, 4), (3, 5)],
        35: [(3, 4), (3, 6), (1, 5), (5, 5)],
        36: [(0, 6), (6, 6), (3, 5)],
        42: [(3, 2), (4, 3)],
        43: [(4, 2), (4, 4), (5, 3)],
        44: [(3, 4), (4, 3)],
        51: [(3, 1), (5, 3)],
        53: [(5, 1), (5, 5), (4, 3), (6, 3)],
        55: [(3, 5), (5, 3)],
        60: [(3, 0), (6, 3)],
        63: [(6, 0), (6, 6), (5, 3)],
        66: [(6, 3), (3, 6)]
    }

    def __init__(self):
        self.board = {}
        self.pawns = {PawnColor.BLACK: set(), PawnColor.WHITE: set()}
        self.prev = {PawnColor.BLACK: None, PawnColor.WHITE: None}

    def get_pawn(self, row, col):
        return self.board.get(row*10+col)

    def add_pawn(self, pawn, row, col):
        if pawn is None:
            return
        idx = row*10+col
        self.board[idx] = pawn

        pawn.coordinates = (row, col)
        self.pawns[pawn.color].add(pawn)

    def remove_pawn(self, pawn):
        if pawn is None:
            return
        del self.board[pawn.coordinates[0]*10+pawn.coordinates[1]]
        self.pawns[pawn.color].discard(pawn)

    def __copy__(self):
        newcopy = empty_copy(self)
        return newcopy

    def copy(self):
        return copy.copy(self)  # copy.deepcopy(self)

    def isMill(self, coordinates_from):
        start_elem = self.board.get(coordinates_from[0]*10+coordinates_from[1])
        if start_elem is None:
            return False
        r = 1
        c = 1
        nums = set()
        for first_step in self.possible_moves[coordinates_from[0]*10+coordinates_from[1]]:
            fse = self.board.get(first_step[0]*10+first_step[1])
            if fse is not None and fse.color == start_elem.color:
                nums.add(first_step)
                for second_step in self.possible_moves[first_step[0]*10+first_step[1]]:
                    sse = self.board.get(second_step[0]*10+second_step[1])
                    if sse is not None and sse.color == start_elem.color and second_step != coordinates_from:
                        nums.add(second_step)

        for item in nums:
            if item[0] == coordinates_from[0]:
                r += 1
            elif item[1] == coordinates_from[1]:
                c += 1

        return (2 if 3 == r and 3 == c else (1 if 3 == r or 3 == c else 0))

    def __str__(self):
        result = ""
        for r in range(7):
            for c in range(7):
                if not (r, c) in self.coordinates:
                    result += "  "
                elif (r, c) in self.coordinates and (r*10+c not in self.board or self.board[r*10+c] is None):
                    result += "+ "
                elif self.board[r*10+c].color == PawnColor.BLACK:
                    result += "C "
                else:
                    result += "W "
            result += "\n"
        return result
