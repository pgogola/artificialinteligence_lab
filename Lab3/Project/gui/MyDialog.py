# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'MyDialog.ui'
#
# Created by: PyQt5 UI code generator 5.12.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MyDialog(object):
    def setupUi(self, MyDialog):
        MyDialog.setObjectName("MyDialog")
        MyDialog.resize(400, 300)
        self.buttonBox = QtWidgets.QDialogButtonBox(MyDialog)
        self.buttonBox.setGeometry(QtCore.QRect(30, 240, 341, 32))
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")

        self.retranslateUi(MyDialog)
        self.buttonBox.accepted.connect(MyDialog.accept)
        self.buttonBox.rejected.connect(MyDialog.reject)
        QtCore.QMetaObject.connectSlotsByName(MyDialog)

    def retranslateUi(self, MyDialog):
        _translate = QtCore.QCoreApplication.translate
        MyDialog.setWindowTitle(_translate("MyDialog", "Dialog"))


