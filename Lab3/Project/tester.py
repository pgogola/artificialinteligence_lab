import numpy as np
import min_max as mm
import Board as bo
import time
import pickle
from game import PlayerAI, Player


def save_data(data, file_name):
    with open("./results/" + file_name + ".dat", "wb") as f:
        pickle.dump(data, f)


def load_data(file_name):
    with open(file_name + ".dat", "rb") as f:
        d = pickle.load(f)
    return d


class TestCase:
    def __init__(self, player1, player2):
        self.player1 = player1
        self.player2 = player2


class PlayerCase:
    def __init__(self, color, isWinner, moves, time, scores, ratting_fun, alg_fun, deepth, alpha_beta_heur=None):
        self.color = color
        self.isWinner = isWinner
        self.moves = moves
        self.time = time
        self.scores = scores
        self.ratting_fun = ratting_fun
        self.deepth = deepth
        self.alg_fun = alg_fun
        self.alpha_beta_heur = alpha_beta_heur


def run_test(ratting_fun_1, alg_fun_1, alpha_beta_fun_1, deepth_1, ratting_fun_2, alg_fun_2, alpha_beta_fun_2, deepth_2):
    player1 = PlayerAI(bo.PawnColor.BLACK, ratting_fun_1,
                       alg_fun_1, deepth_1, alpha_beta_fun_1)
    player2 = PlayerAI(bo.PawnColor.WHITE, ratting_fun_2,
                       alg_fun_2, deepth_2, alpha_beta_fun_2)
    board = bo.Board()

    winner = None
    winner_moves = 0

    player1_time = 0
    player2_time = 0
    player1_scores = []
    player2_scores = []

    p1 = PlayerCase(player1.color, False, player1.movements, player1_time, player1_scores, player1.state_ratting.__name__,
                    player1.algorithm.__name__, player1.max_depth, player1.alpha_beta_heur.__name__ if player1.alpha_beta_heur is not None else None)

    p2 = PlayerCase(player2.color, False, player2.movements, player2_time, player2_scores, player2.state_ratting.__name__,
                    player2.algorithm.__name__, player2.max_depth, player2.alpha_beta_heur.__name__ if player2.alpha_beta_heur is not None else None)

    while not (player1.lost or player2.lost):

        start = time.time()
        board, scores = player1.move(board)
        stop = time.time()
        player1_scores.append(scores)
        player1_time += (stop-start)
        print(board)
        if player1.lost:
            p2.isWinner = True
            break

        start = time.time()
        board, scores = player2.move(board)
        stop = time.time()
        player2_scores.append(scores)
        player2_time += (stop-start)
        print(board)
        if player2.lost:
            p1.isWinner = True
            break
        input()
        if(player1.movements*player2.movements > 64 and len(board.pawns[bo.PawnColor.BLACK]) <= 3 and len(board.pawns[bo.PawnColor.WHITE]) <= 3):
            break

        p1.time = player1_time
        p2.time = player2_time
        p1.moves = player1.movements
        p2.moves = player2.movements

    return TestCase(p1, p2)


def do_tests():
    number_of_tests = 5
    depth = np.arange(2, 5)
    algorithms = [mm.min_max, mm.alpha_beta]
    ratting_functions = [mm.state_rating_pieces, mm.state_rating_blocked]
    alpha_beta_heurestics = [
        mm.alpha_beta_sort_mill_number, mm.alpha_beta_sort_pieces_number]

    # res = run_test(mm.state_rating_blocked, mm.alpha_beta, mm.alpha_beta_sort_mill_number, 5,
    #                mm.state_rating_pieces, mm.alpha_beta, mm.alpha_beta_sort_pieces_number, 5)

    for d in depth:
        results = []
        for test_num in range(number_of_tests):
            res = run_test(mm.state_rating_pieces, mm.min_max, None, 3,
                           mm.state_rating_pieces, mm.min_max, None, 3)
            print("A")
            results.append(res)

        save_data(results, "min_max_only_same_depth_dif_ratting"+str(d))

    for alg in alpha_beta_heurestics:
        for d in depth:
            results = []
            for test_num in range(number_of_tests):
                res = run_test(mm.state_rating_pieces, mm.alpha_beta, alg, d,
                               mm.state_rating_pieces, mm.min_max, None, d)
                print("B")
                results.append(res)

            save_data(results, "alphabeta_minmax_same_depth_same_ratting"+str(d))

    for d in depth:
        results = []
        for test_num in range(number_of_tests):
            res = run_test(mm.state_rating_pieces, mm.alpha_beta, alpha_beta_heurestics[0], d,
                           mm.state_rating_pieces, mm.alpha_beta, alpha_beta_heurestics[1], d)
            results.append(res)
            print("C")

        save_data(results, "alphabeta_only_same_depth_same_ratting_dif_heur"+str(d))

    for d in depth:
        results = []
        for test_num in range(number_of_tests):
            res = run_test(mm.state_rating_blocked, mm.alpha_beta, alpha_beta_heurestics[0], d,
                           mm.state_rating_pieces, mm.alpha_beta, alpha_beta_heurestics[1], d)
            results.append(res)
            print("D")
        save_data(results, "alphabeta_only_same_depth_diff_ratting_dif_heur"+str(d))

    for d in depth:
        results = []
        for test_num in range(number_of_tests):
            res = run_test(mm.state_rating_pieces, mm.alpha_beta, alpha_beta_heurestics[0], d,
                           mm.state_rating_blocked, mm.alpha_beta, alpha_beta_heurestics[1], d)
            results.append(res)
            print("E")
        save_data(
            results, "alphabeta_only_same_depth_diff_ratting2_dif_heur"+str(d))

    for d in depth:
        results = []
        for test_num in range(number_of_tests):
            res = run_test(mm.state_rating_pieces, mm.alpha_beta, alpha_beta_heurestics[0], d,
                           mm.state_rating_pieces, mm.alpha_beta, alpha_beta_heurestics[0], 4)
            results.append(res)
            print("F")
        save_data(results, "alphabeta_thesame_diff_deepth"+str(d))

    for d in depth:
        results = []
        for test_num in range(number_of_tests):
            res = run_test(mm.state_rating_blocked, mm.alpha_beta, alpha_beta_heurestics[1], d,
                           mm.state_rating_blocked, mm.alpha_beta, alpha_beta_heurestics[1], 4)
            results.append(res)
            print("G")
        save_data(results, "alphabeta_thesame_diff_deepth"+str(d))


do_tests()
